import  { Routes, RouterModule} from '@angular/router';
import  {Layout2Component} from "./layout2/layout2.component";
import {DashboardComponent} from "./layout2/login/dashboard/dashboard.component";
import {Layout1Component} from "./layout1/layout1.component";
import {LoginComponent} from "./layout2/login/login.component";
import {RegisterComponent} from "./layout2/register/register.component";
import {AdminComponent} from "./layout2/register/admin/admin.component";
import {InstructdbComponent} from "./layout2/login/contentarea/instructdb/instructdb.component";
import {StaffdbComponent} from "./layout2/login/contentarea/staffdb/staffdb.component";
import {AdmindbComponent} from "./layout2/login/contentarea/admindb/admindb.component";
import {ViewteacherComponent} from "./layout2/login/contentarea/admindb/adminca/viewteacher/viewteacher.component";
import {ViewstdComponent} from "./layout2/login/contentarea/admindb/adminca/viewstd/viewstd.component";
import {AdmincaComponent} from "./layout2/login/contentarea/admindb/adminca/adminca.component";
import {InstructorComponent} from "./layout2/login/contentarea/admindb/adminca/instructor/instructor.component";
import {InstructcaComponent} from "./layout2/login/contentarea/instructdb/instructca/instructca.component";
import {PublishresultComponent} from "./layout2/login/contentarea/instructdb/instructca/publishresult/publishresult.component";
import {ManageexamsComponent} from "./layout2/login/contentarea/instructdb/instructca/manageexams/manageexams.component";
import {SectionsComponent} from "./layout2/login/contentarea/instructdb/instructca/sections/sections.component";
import {HttptestComponent} from "./layout1/httptest/httptest.component";
import {AddcourseComponent} from "./layout2/login/contentarea/admindb/adminca/addcourse/addcourse.component";
import {AssigncourseComponent} from "./layout2/login/contentarea/admindb/adminca/assigncourse/assigncourse.component";
import {ManagecourseComponent} from "./layout2/login/contentarea/admindb/adminca/managecourse/managecourse.component";
import {AddclassComponent} from "./layout2/login/contentarea/admindb/adminca/addclass/addclass.component";
import {EditclassComponent} from "./layout2/login/contentarea/admindb/adminca/editclass/editclass.component";
import {SpecifycoursesComponent} from "./layout2/login/contentarea/admindb/adminca/specifycourses/specifycourses.component";
import {ClasswallComponent} from "./layout2/login/contentarea/admindb/adminca/classwall/classwall.component";
import {ViewstaffComponent} from "./layout2/login/contentarea/admindb/adminca/viewstaff/viewstaff.component";
import {AdminpostComponent} from "./layout2/login/contentarea/admindb/adminca/adminpost/adminpost.component";
import {StudentComponent} from "./layout2/login/contentarea/admindb/adminca/student/student.component";
import {StaffComponent} from "./layout2/login/contentarea/admindb/adminca/staff/staff.component";
import {ManagefeesComponent} from "./layout2/login/contentarea/admindb/adminca/managefees/managefees.component";
import {DatesheetComponent} from "./layout2/login/contentarea/admindb/adminca/datesheet/datesheet.component";
import {InstructdatesheetComponent} from "./layout2/login/contentarea/instructdb/instructca/instructdatesheet/instructdatesheet.component";
import {SetpaperComponent} from "./layout2/login/contentarea/instructdb/instructca/setpaper/setpaper.component";
import {AssignClassToTeacherComponent} from "./layout2/login/contentarea/admindb/adminca/assign-class-to-teacher/assign-class-to-teacher.component";
import {ViewresultsComponent} from "./layout2/login/contentarea/admindb/adminca/viewresults/viewresults.component";
import {InstructviewresultComponent} from "./layout2/login/contentarea/instructdb/instructca/publishresult/instructviewresult/instructviewresult.component";
import {ApproveresultsComponent} from "./layout2/login/contentarea/admindb/adminca/approveresults/approveresults.component";
import {StudentdbComponent} from "./layout2/login/contentarea/studentdb/studentdb.component";
import {StudentcaComponent} from "./layout2/login/contentarea/studentdb/studentca/studentca.component";
import {ResultsComponent} from "./layout2/login/contentarea/studentdb/studentca/results/results.component";
import {AnnounceComponent} from "./layout2/login/contentarea/studentdb/studentca/announce/announce.component";
import {DsComponent} from "./layout2/login/contentarea/studentdb/studentca/ds/ds.component";
import {InvigilatorComponent} from "./layout2/login/contentarea/admindb/adminca/invigilator/invigilator.component";
import {SetinvComponent} from "./layout2/login/contentarea/admindb/adminca/setinv/setinv.component";
import {NotifyComponent} from "./layout2/login/contentarea/instructdb/instructca/notify/notify.component";
import {ViewsheetComponent} from "./layout2/login/contentarea/admindb/adminca/viewsheet/viewsheet.component";
import {DefaulterComponent} from "./layout2/login/contentarea/admindb/adminca/defaulter/defaulter.component";
import {InvoiceComponent} from "./layout2/login/contentarea/admindb/adminca/invoice/invoice.component";
import {AnnouncementsComponent} from "./layout2/login/contentarea/admindb/adminca/announcements/announcements.component";
import {AnnComponent} from "./layout2/login/contentarea/instructdb/instructca/ann/ann.component";

const APP_ROUTES: Routes = [

  {path : 'layout1' , component: Layout1Component},

   {path: 'admindb', component:AdmindbComponent, children:[

     {path: 'adminca' , component: AdmincaComponent, children:[

       {path: 'instructor' , component: InstructorComponent},

       {path:'viewresults', component: ViewresultsComponent},

       {path:'approveresults', component: ApproveresultsComponent},

       {path: 'assigncourse', component: AssigncourseComponent},

       {path: 'addcourse', component: AddcourseComponent},

       {path: 'managecourse', component: ManagecourseComponent},

       {path: 'addclass', component: AddclassComponent},

       {path: 'editclass', component: EditclassComponent},

       {path: 'specifycourses',component: SpecifycoursesComponent},

       {path:'viewstd', component: ViewstdComponent},

       {path:'viewteacher', component: ViewteacherComponent},

       {path:'viewstaff', component: ViewstaffComponent},

       {path:'adminpost', component: AdminpostComponent},

       {path:'student', component: StudentComponent},

       {path:'staff', component: StaffComponent},

       {path:'managefees', component: ManagefeesComponent},

       {path:'datesheet', component: DatesheetComponent},

       {path:'invigilator', component: InvigilatorComponent},

       {path:'setinv', component: SetinvComponent},

       {path:'viewsheet', component: ViewsheetComponent},

       {path:'defaulter', component: DefaulterComponent},

       {path:'invoice', component: InvoiceComponent},

       {path:'announcements', component: AnnouncementsComponent},

       {path:'assignclasstoteacher', component: AssignClassToTeacherComponent},


     ]},

     ]},


    {path : '', redirectTo : '/layout1', pathMatch : 'full'},

    {path: 'instructdb' , component: InstructdbComponent, children:[

      {path: 'instructca' , component: InstructcaComponent, children:[

        {path: 'manageexams' , component: ManageexamsComponent},

        {path: 'notify',component: NotifyComponent},

        {path: 'ann',component: AnnComponent},

        {path: 'setpaper', component: SetpaperComponent},

        {path: 'publishresult' , component: PublishresultComponent, children:[

          {path: 'instructviewresult' , component: InstructviewresultComponent},


        ]},

        {path: 'sections' , component: SectionsComponent},

        {path: 'instructdatesheet', component: InstructdatesheetComponent},

      ]}
      ]},


    {path: 'adminca' , component: AdmincaComponent, children:[

      {path: 'viewstd' , component: ViewstdComponent},

      {path: 'viewteacher' , component: ViewteacherComponent},

    ]},


  {path: 'studentdb', component:StudentdbComponent, children:[

    {path: 'studentca' , component: StudentcaComponent, children:[

      {path: 'results' , component: ResultsComponent},

      {path: 'ds' , component: DsComponent},

      {path: 'announce' , component: AnnounceComponent}

    ]},

  ]},

];

export const routing = RouterModule.forRoot(APP_ROUTES);
