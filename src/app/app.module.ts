import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {routing} from "./app.routing";
import {RouterModule} from '@angular/router';
import {ServicesService} from './layout2/register/services.service';

import { AppComponent } from './app.component';
import { Layout1Component } from './layout1/layout1.component';
import { Layout2Component } from './layout2/layout2.component';
import { LoginComponent } from './layout2/login/login.component';
import { RegisterComponent } from './layout2/register/register.component';
import { DashboardComponent } from './layout2/login/dashboard/dashboard.component';
import {InstructorComponent} from "./layout2/login/contentarea/admindb/adminca/instructor/instructor.component";
import { AdminComponent } from './layout2/register/admin/admin.component';
import { HomeComponent } from './layout1/home/home.component';
import { ContentareaComponent } from './layout2/login/contentarea/contentarea.component';
import { StaffdbComponent } from './layout2/login/contentarea/staffdb/staffdb.component';
import { InstructdbComponent } from './layout2/login/contentarea/instructdb/instructdb.component';
import { FooterComponent } from './layout2/login/dashboard/footer/footer.component';
import { HeaderComponent } from './layout2/login/dashboard/header/header.component';
import { AdminlpComponent } from './layout2/login/dashboard/adminlp/adminlp.component';
import { AdmindbComponent } from './layout2/login/contentarea/admindb/admindb.component';
import { AdmincaComponent } from './layout2/login/contentarea/admindb/adminca/adminca.component';
import { ViewstdComponent } from './layout2/login/contentarea/admindb/adminca/viewstd/viewstd.component';
import { ViewteacherComponent } from './layout2/login/contentarea/admindb/adminca/viewteacher/viewteacher.component';
import { LeftpaneComponent } from './layout2/login/contentarea/admindb/leftpane/leftpane.component';
import { InstructcaComponent } from './layout2/login/contentarea/instructdb/instructca/instructca.component';
import { InstructLeftpaneComponent } from './layout2/login/contentarea/instructdb/instruct-leftpane/instruct-leftpane.component';
import { PublishresultComponent } from './layout2/login/contentarea/instructdb/instructca/publishresult/publishresult.component';
import { ManageexamsComponent } from './layout2/login/contentarea/instructdb/instructca/manageexams/manageexams.component';
import { SectionsComponent } from './layout2/login/contentarea/instructdb/instructca/sections/sections.component';
import { HttptestComponent } from './layout1/httptest/httptest.component';
import {TestingService} from "./layout1/testing.service";
import { ViewstaffComponent } from './layout2/login/contentarea/admindb/adminca/viewstaff/viewstaff.component';
import { AddcourseComponent } from './layout2/login/contentarea/admindb/adminca/addcourse/addcourse.component';
import {AddcourseServiceService} from "./layout2/login/contentarea/admindb/adminca/addcourse/addcourse-service.service";
import { AssigncourseComponent } from './layout2/login/contentarea/admindb/adminca/assigncourse/assigncourse.component';
import {AssigncourseserviceService} from "./layout2/login/contentarea/admindb/adminca/assigncourse/assigncourseservice.service";
import { ManagecourseComponent } from './layout2/login/contentarea/admindb/adminca/managecourse/managecourse.component';
import {ManagecourseserviceService} from "./layout2/login/contentarea/admindb/adminca/managecourse/managecourseservice.service";
import { AddclassComponent } from './layout2/login/contentarea/admindb/adminca/addclass/addclass.component';
import {AddclassserviceService} from "./layout2/login/contentarea/admindb/adminca/addclass/addclassservice.service";
import { EditclassComponent } from './layout2/login/contentarea/admindb/adminca/editclass/editclass.component';
import {EditclassserviceService} from "./layout2/login/contentarea/admindb/adminca/editclass/editclassservice.service";
import { SpecifycoursesComponent } from './layout2/login/contentarea/admindb/adminca/specifycourses/specifycourses.component';
import {SpecifycoursesService} from "./layout2/login/contentarea/admindb/adminca/specifycourses/specifycourses.service";
import { ClasswallComponent } from './layout2/login/contentarea/admindb/adminca/classwall/classwall.component';
import {ClasswallserviceService} from "./layout2/login/contentarea/admindb/adminca/classwall/classwallservice.service";
import {ViewstaffserviceService} from "./layout2/login/contentarea/admindb/adminca/viewstaff/viewstaffservice.service";
import {ViewteacherserviceService} from "./layout2/login/contentarea/admindb/adminca/viewteacher/viewteacherservice.service";
import {ViewstdserviceService} from "./layout2/login/contentarea/admindb/adminca/viewstd/viewstdservice.service";
import { AdminpostComponent } from './layout2/login/contentarea/admindb/adminca/adminpost/adminpost.component';
import {AdminpostserviceService} from "./layout2/login/contentarea/admindb/adminca/adminpost/adminpostservice.service";
import { StudentComponent } from './layout2/login/contentarea/admindb/adminca/student/student.component';
import { StaffComponent } from './layout2/login/contentarea/admindb/adminca/staff/staff.component';
import {InstructorserviceService} from "./layout2/login/contentarea/admindb/adminca/instructor/instructorservice.service";
import {StudentserviceService} from "./layout2/login/contentarea/admindb/adminca/student/studentservice.service";
import {StaffserviceService} from "./layout2/login/contentarea/admindb/adminca/staff/staffservice.service";
import { ManagefeesComponent } from './layout2/login/contentarea/admindb/adminca/managefees/managefees.component';
import { DatesheetComponent } from './layout2/login/contentarea/admindb/adminca/datesheet/datesheet.component';
import { InstructdatesheetComponent } from './layout2/login/contentarea/instructdb/instructca/instructdatesheet/instructdatesheet.component';
import { SetpaperComponent } from './layout2/login/contentarea/instructdb/instructca/setpaper/setpaper.component';
import {LoginService} from "./layout2/login/login.service";
import {PublishresultService} from "./layout2/login/contentarea/instructdb/instructca/publishresult/publishresult.service";
import { AssignClassToTeacherComponent } from './layout2/login/contentarea/admindb/adminca/assign-class-to-teacher/assign-class-to-teacher.component';
import {AssignclasstoteacherService} from "./layout2/login/contentarea/admindb/adminca/assign-class-to-teacher/assignclasstoteacher.service";
import {InstructdbService} from "./layout2/login/contentarea/instructdb/instructdb.service";
import {AdmindbService} from "./layout2/login/contentarea/admindb/admindb.service";
import { ViewresultsComponent } from './layout2/login/contentarea/admindb/adminca/viewresults/viewresults.component';
import {ViewresultsService} from "./layout2/login/contentarea/admindb/adminca/viewresults/viewresults.service";
import { InstructviewresultComponent } from './layout2/login/contentarea/instructdb/instructca/publishresult/instructviewresult/instructviewresult.component';
import {InstructviewresultService} from "./layout2/login/contentarea/instructdb/instructca/publishresult/instructviewresult.service";
import { ApproveresultsComponent } from './layout2/login/contentarea/admindb/adminca/approveresults/approveresults.component';
import {ApproveresultsService} from "./layout2/login/contentarea/admindb/adminca/approveresults/approveresults.service";
import { StudentdbComponent } from './layout2/login/contentarea/studentdb/studentdb.component';
import { StudentcaComponent } from './layout2/login/contentarea/studentdb/studentca/studentca.component';
import { ResultsComponent } from './layout2/login/contentarea/studentdb/studentca/results/results.component';
import { StdheaderComponent } from './layout2/login/dashboard/stdheader/stdheader.component';
import { StdleftpaneComponent } from './layout2/login/contentarea/studentdb/stdleftpane/stdleftpane.component';
import { AnnounceComponent } from './layout2/login/contentarea/studentdb/studentca/announce/announce.component';
import { DsComponent } from './layout2/login/contentarea/studentdb/studentca/ds/ds.component';
import {StudentdbService} from "./layout2/login/contentarea/studentdb/studentdb.service";
import {StudentcaService} from "./layout2/login/contentarea/studentdb/studentca/studentca.service";
import {ResultsService} from "./layout2/login/contentarea/studentdb/studentca/results/results.service";
import {DsService} from "./layout2/login/contentarea/studentdb/studentca/ds/ds.service";
import {AnnounceService} from "./layout2/login/contentarea/studentdb/studentca/announce/announce.service";
import {ViewService} from "./layout2/login/contentarea/admindb/adminca/datesheet/view.service";
import { InvigilatorComponent } from './layout2/login/contentarea/admindb/adminca/invigilator/invigilator.component';
import {InvigilatorService} from "./layout2/login/contentarea/admindb/adminca/invigilator/invigilator.service";
import { SetinvComponent } from './layout2/login/contentarea/admindb/adminca/setinv/setinv.component';
import {SetinvService} from "./layout2/login/contentarea/admindb/adminca/setinv/setinv.service";
import { NotifyComponent } from './layout2/login/contentarea/instructdb/instructca/notify/notify.component';
import {NotifyService} from "./layout2/login/contentarea/instructdb/instructca/notify/notify.service";
import { ViewsheetComponent } from './layout2/login/contentarea/admindb/adminca/viewsheet/viewsheet.component';
import {ViewsheetService} from "./layout2/login/contentarea/admindb/adminca/viewsheet/viewsheet.service";
import {FeesserviceService} from "./layout2/login/contentarea/admindb/adminca/managefees/feesservice.service";
import { DefaulterComponent } from './layout2/login/contentarea/admindb/adminca/defaulter/defaulter.component';
import {DefaulterService} from "./layout2/login/contentarea/admindb/adminca/defaulter/defaulter.service";
import { InvoiceComponent } from './layout2/login/contentarea/admindb/adminca/invoice/invoice.component';
import {InvoiceService} from "./layout2/login/contentarea/admindb/adminca/invoice/invoice.service";
import {HeaderService} from "./layout2/login/dashboard/header/header.service";
import {AdminleftpaneService} from "./layout2/login/contentarea/admindb/leftpane/adminleftpane.service";
import { AnnouncementsComponent } from './layout2/login/contentarea/admindb/adminca/announcements/announcements.component';
import {AnnouncementsService} from "./layout2/login/contentarea/admindb/adminca/announcements/announcements.service";
import { AnnComponent } from './layout2/login/contentarea/instructdb/instructca/ann/ann.component';
import {AnnService} from "./layout2/login/contentarea/instructdb/instructca/ann/ann.service";
import {SetpaperService} from "./layout2/login/contentarea/instructdb/instructca/setpaper/setpaper.service";
import {InstructLeftpaneService} from "./layout2/login/contentarea/instructdb/instruct-leftpane/instruct-leftpane.service";


@NgModule({
  declarations: [
    AppComponent,
    Layout1Component,
    Layout2Component,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    InstructorComponent,
    AdminComponent,
    HomeComponent,
    StaffdbComponent,
    InstructdbComponent,
    ContentareaComponent,
    FooterComponent,
    HeaderComponent,
    AdminlpComponent,
    AdmindbComponent,
    AdmincaComponent,
    ViewstdComponent,
    ViewteacherComponent,
    LeftpaneComponent,
    InstructcaComponent,
    InstructLeftpaneComponent,
    PublishresultComponent,
    ManageexamsComponent,
    SectionsComponent,
    HttptestComponent,
    ViewstaffComponent,
    AddcourseComponent,
    AssigncourseComponent,
    ManagecourseComponent,
    AddclassComponent,
    EditclassComponent,
    SpecifycoursesComponent,
    ClasswallComponent,
    AdminpostComponent,
    StudentComponent,
    StaffComponent,
    ManagefeesComponent,
    DatesheetComponent,
    InstructdatesheetComponent,
    SetpaperComponent,
    AssignClassToTeacherComponent,
    ViewresultsComponent,
    InstructviewresultComponent,
    ApproveresultsComponent,
    StudentdbComponent,
    StudentcaComponent,
    ResultsComponent,
    StdheaderComponent,
    StdleftpaneComponent,
    AnnounceComponent,
    DsComponent,
    InvigilatorComponent,
    SetinvComponent,
    NotifyComponent,
    ViewsheetComponent,
    DefaulterComponent,
    InvoiceComponent,
    AnnouncementsComponent,
    AnnComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
  ],
  providers: [ServicesService, RouterModule, TestingService,AddcourseServiceService,AssigncourseserviceService,ManagecourseserviceService,
  AddclassserviceService, EditclassserviceService, SpecifycoursesService, ClasswallserviceService, ViewstaffserviceService,
    ViewstdserviceService, ViewteacherserviceService, AdminpostserviceService, InstructorserviceService, StudentserviceService
  , StaffserviceService, LoginService, PublishresultService, AssignclasstoteacherService, InstructdbService, AdmindbService, ViewresultsService
      , InstructviewresultService, ApproveresultsService, StudentdbService, StudentcaService, ResultsService, DsService, AnnounceService,
        ViewService, InvigilatorService, SetinvService,NotifyService, ViewsheetService, FeesserviceService, DefaulterService, InvoiceService,
        HeaderService, AdminleftpaneService, AnnouncementsService, AnnService, SetpaperService, InstructLeftpaneService],

  bootstrap: [AppComponent]
})
export class AppModule { }
