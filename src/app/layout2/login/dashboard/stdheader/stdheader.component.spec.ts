import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StdheaderComponent } from './stdheader.component';

describe('StdheaderComponent', () => {
  let component: StdheaderComponent;
  let fixture: ComponentFixture<StdheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StdheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StdheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
