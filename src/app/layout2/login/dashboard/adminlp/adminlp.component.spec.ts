import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminlpComponent } from './adminlp.component';

describe('AdminlpComponent', () => {
  let component: AdminlpComponent;
  let fixture: ComponentFixture<AdminlpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminlpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminlpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
