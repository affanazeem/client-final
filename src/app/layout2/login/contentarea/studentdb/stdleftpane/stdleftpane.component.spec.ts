import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StdleftpaneComponent } from './stdleftpane.component';

describe('StdleftpaneComponent', () => {
  let component: StdleftpaneComponent;
  let fixture: ComponentFixture<StdleftpaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StdleftpaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StdleftpaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
