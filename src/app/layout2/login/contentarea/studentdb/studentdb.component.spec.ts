import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentdbComponent } from './studentdb.component';

describe('StudentdbComponent', () => {
  let component: StudentdbComponent;
  let fixture: ComponentFixture<StudentdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
