import { Component, OnInit } from '@angular/core';
import {TestingService} from "../../../../layout1/testing.service";
import {Router} from "@angular/router";
import * as $ from 'jquery';
import {StudentdbService} from "./studentdb.service";


@Component({
  selector: 'app-studentdb',
  templateUrl: './studentdb.component.html',
  styleUrls: ['./studentdb.component.css']
})
export class StudentdbComponent implements OnInit {

  firstname:any;
  lastname:any;
  classname: any;
  array:any = [];

  constructor(private _student:TestingService, private router: Router, private _httpService: StudentdbService) {

  _student.getDetails().subscribe(
    data => {

      this.array = data;
      this.firstname = this.array.firstname;
      this.lastname = this.array.lastname;
      this.classname = this.array.classId;
      console.log(this.firstname, this.classname);
      this._httpService.setClassName(this.firstname, this.lastname,this.classname);
      },
  )
  }



  ngOnInit() {

  }


  res(){
    this.router.navigate(['/studentdb/studentca/results'])
  }


  datesheet(){
    this.router.navigate(['/studentdb/studentca/ds'])
  }


  announce(){
    this.router.navigate(['/studentdb/studentca/announce'])
  }




}
