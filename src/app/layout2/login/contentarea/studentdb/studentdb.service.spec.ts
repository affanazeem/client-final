import { TestBed, inject } from '@angular/core/testing';

import { StudentdbService } from './studentdb.service';

describe('StudentdbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentdbService]
    });
  });

  it('should ...', inject([StudentdbService], (service: StudentdbService) => {
    expect(service).toBeTruthy();
  }));
});
