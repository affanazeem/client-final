import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-studentca',
  templateUrl: './studentca.component.html',
  styleUrls: ['./studentca.component.css']
})
export class StudentcaComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $(".login-html").fadeOut();
    $(".login-html").hide();
    $(".main").fadeOut();
    $(".main").hide();

  }

}
