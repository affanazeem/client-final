import { Component, OnInit } from '@angular/core';
import {TestingService} from "../../../../../../layout1/testing.service";
import {ResultsService} from "./results.service";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  array: any = [];
  percentage: any;
  m : any;


  array_res:any = [];

  v: any = {subjectname:'',marks:'',grade:''}
  constructor(private _student: TestingService, private _httpService: ResultsService) {

    _student.getDetails().subscribe(
      data => {

        this.array = data;
        console.log(this.array);

      },

      error =>{},

      ()=>{

        this._httpService.viewResults(this.array.firstname,this.array.lastname).subscribe(

          data =>{
            this.array_res = data;
            console.log(this.array_res);

            for(var i = 0 ; i < this.array_res.length; i++){

              this.m = parseInt(this.array_res[i].marks);
              this.m = parseInt("" + this.array_res[i].marks, 0);
              this.percentage = (this.m/100);
              console.log(this.percentage);
            }

            },

          error=>{},

          ()=>{}

        )


      }
    )
  }

  ngOnInit() {

  }


}
