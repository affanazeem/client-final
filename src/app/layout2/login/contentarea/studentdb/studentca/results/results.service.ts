import { Injectable } from '@angular/core';
import {Response, Http, Headers} from '@angular/http';

@Injectable()
export class ResultsService {


  constructor(private _http:Http) { }

  viewResults(firstname: any, lastname: any) {
    console.log('in service: '+firstname+lastname);
    let url = 'http://localhost:3000/students/allResults/' + firstname + '/' + lastname;
    return this._http.get(url).map((response: Response) => response.json());
  }

}
