import { Component, OnInit } from '@angular/core';
import {StudentdbService} from "../../studentdb.service";
import {DsService} from "./ds.service";

@Component({
  selector: 'app-ds',
  templateUrl: './ds.component.html',
  styleUrls: ['./ds.component.css']
})
export class DsComponent implements OnInit {

  classname: any;
  array_ds: any = [];

  constructor(private std: StudentdbService, private _httpService: DsService) {

    this.classname = std.classname;
    console.log("class name"+this.classname);
  }

  ngOnInit() {

    this._httpService.setDS(this.classname).subscribe(

      data =>{this.array_ds = data},

      error =>{},

      ()=>{}

    )
  }

}
