import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";

@Injectable()
export class DsService {

  constructor(private _http:Http) { }


  setDS(classname: any){

      let url = 'http://localhost:3000/admins/classWiseDateSheet/' + classname;
      return this._http.get(url).map((response: Response) => response.json());


  }

}
