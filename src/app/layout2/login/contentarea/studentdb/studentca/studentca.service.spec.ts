import { TestBed, inject } from '@angular/core/testing';

import { StudentcaService } from './studentca.service';

describe('StudentcaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentcaService]
    });
  });

  it('should ...', inject([StudentcaService], (service: StudentcaService) => {
    expect(service).toBeTruthy();
  }));
});
