import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentcaComponent } from './studentca.component';

describe('StudentcaComponent', () => {
  let component: StudentcaComponent;
  let fixture: ComponentFixture<StudentcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
