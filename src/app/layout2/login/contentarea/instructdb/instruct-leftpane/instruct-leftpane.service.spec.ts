import { TestBed, inject } from '@angular/core/testing';

import { InstructLeftpaneService } from './instruct-leftpane.service';

describe('InstructLeftpaneService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstructLeftpaneService]
    });
  });

  it('should ...', inject([InstructLeftpaneService], (service: InstructLeftpaneService) => {
    expect(service).toBeTruthy();
  }));
});
