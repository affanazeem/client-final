import { Component, OnInit } from '@angular/core';
import {InstructLeftpaneService} from "./instruct-leftpane.service";
import {InstructdbService} from "../instructdb.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-instruct-leftpane',
  templateUrl: './instruct-leftpane.component.html',
  styleUrls: ['./instruct-leftpane.component.css']
})
export class InstructLeftpaneComponent implements OnInit {

  username: any;
  password: any;

  array_login: any=[];

  constructor(private _httpService: InstructLeftpaneService, private _instructdb: InstructdbService, private router: Router) {

     }

  ngOnInit() {

    this._httpService.getName().subscribe(
      data =>{this.array_login = data
              this.username = this.array_login.username;
              this.password = this.array_login.password;
              console.log('username logout: '+this.username);
      },
      error =>{},
      () => {}
    )
  }

  logout() {
    console.log('logout!'+this.username+this.password);
    this._httpService.logoutTeacher(this.username,this.password).subscribe(

      data =>{},

    error =>{},

      () =>{

        this.router.navigate(['/layout1']);

      },

    )

  }
}
