import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructLeftpaneComponent } from './instruct-leftpane.component';

describe('InstructLeftpaneComponent', () => {
  let component: InstructLeftpaneComponent;
  let fixture: ComponentFixture<InstructLeftpaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructLeftpaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructLeftpaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
