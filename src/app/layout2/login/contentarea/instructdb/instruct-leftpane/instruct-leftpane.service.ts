import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
@Injectable()
export class InstructLeftpaneService {

  constructor(private _http:Http) { }


  getName()
  {
    let url = 'http://localhost:3000/teachers/getname';
    return this._http.get(url).map((response: Response) => response.json());
  }

  logoutTeacher(username:any,password:any){
    let url = 'http://localhost:3000/teachers/logoutteacher/'+username+'/'+password;
    return this._http.delete(url).map((response:Response) => response.json());
  }
}

