import { TestBed, inject } from '@angular/core/testing';

import { InstructdbService } from './instructdb.service';

describe('InstructdbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstructdbService]
    });
  });

  it('should ...', inject([InstructdbService], (service: InstructdbService) => {
    expect(service).toBeTruthy();
  }));
});
