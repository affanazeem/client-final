import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import {InstructdbService} from "./instructdb.service";
import {TestingService} from "../../../../layout1/testing.service";


@Component({
  selector: 'app-instructdb',
  templateUrl: './instructdb.component.html',
  styleUrls: ['./instructdb.component.css']
})
export class InstructdbComponent implements OnInit {

  firstname: any;
  classname: any;
  subjectname: any;
  array:any = [];
  password: any;
  array_login: any = [];

  constructor(private _httpService: InstructdbService, private _testing: TestingService, private router: Router) {
   this.firstname = _testing.username;
    this.password = _testing.password;
  }

  ngOnInit() {

    this._httpService.Login().subscribe(

      data =>{
        this.array_login = data
        this.firstname = this.array_login.username;
        console.log("firstame"+this.firstname);
        this.password = this.array_login.password;

        console.log(this.firstname);
        this._httpService.login(this.firstname,this.password);
        this._httpService.getAllClasses(this.firstname).subscribe(
          data => {
            this.array = data
          }
        );

      },

      error =>{},

      () =>{}

    )
  }

  results(v: any){
    this._httpService.usedetails(v);
    console.log('in result method!');
    $(".modal_faculty").fadeOut();
        $(".login-html").fadeOut();
        $(".login-html").hide();
        $(".main").fadeOut();
        $(".main").hide();
    this.router.navigate(['/instructdb/instructca/publishresult']);
  }

  post(v: any){

    this._httpService.getAllResult(v).subscribe(
      data=>{},
      error=>{},
      ()=> {
        $(".call_modal_faculty").show(function () {
          $(".modal_faculty").fadeIn();
        })

      }
      )
  }


  ok(){

    $(".btn-danger").click(function () {
      $(".modal_faculty").fadeOut();
    });

  }


  alerts(){
    console.log('in alert');

    $(".login-html").fadeOut();
    $(".login-html").hide();
    $(".main").fadeOut();
    $(".main").hide();
    this.router.navigate(['/instructdb/instructca/notify']);
  }


  setpaper(v: any){
    this._httpService.usedetails(v);

    console.log('in setpaper');
    $(".login-html").fadeOut();
    $(".login-html").hide();
    $(".main").fadeOut();
    $(".main").hide();
    this.router.navigate(['/instructdb/instructca/setpaper']);
  }

}
