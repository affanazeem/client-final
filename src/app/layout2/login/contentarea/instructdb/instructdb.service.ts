import { Injectable } from '@angular/core';
import {Response, Http, Headers} from '@angular/http';

@Injectable()
export class InstructdbService {

  firstname: any;
  classname: any;
  sectionname: any;
  subjectname: any;

  username: any;
  password: any;
  initial: boolean;

  loginUser:any = {username:'',password: ''};

  constructor(private _http:Http) { }

  login(username:any,password:any){
    this.username = username;
    this.password = password;
    this.initial = false;
    console.log("Instruct DB: "+this.username + this.password);
    this.loginUser.firstname = this.username;
    this.loginUser.word = this.password;
  }

  getAllClasses(firstname: any){
    console.log('okkk');
    return this._http.get('http://localhost:3000/admins/InstructorClassAndCourse/'+firstname).map((response:Response) => response.json());
  }

  usedetails(v: any){
    this.firstname = v.firstname;
    this.classname = v.classname;
    this.subjectname = v.subjectname;
    this.sectionname = v.sectionname;

    console.log(this.classname+this.subjectname);
  }

  getAllResult(v: any){

    this.classname = v.classname;
    this.subjectname = v.subjectname;
    this.sectionname = v.sectionname;
    this.firstname = v.firstname;

    console.log(this.classname);
    console.log(this.sectionname);
    console.log(this.subjectname);

   console.log("in notification!");

    var body = {
      to:"Admin",
      from: this.firstname,
      classname: this.classname,
      sectionname:this.sectionname,
      subjectname:this.subjectname,
      type:"result",
      expire:false
    };
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/teachers/sendNotification', body,
      {
        headers: headers,
      }).map(res => res.json())

  }

  Login(){
    console.log('okkk');
    var post="teacher"
    var expire = "true";
    return this._http.get('http://localhost:3000/teachers/getLoginTeacher/'+post+'/'+expire).map((response:Response) => response.json());
  }


}
