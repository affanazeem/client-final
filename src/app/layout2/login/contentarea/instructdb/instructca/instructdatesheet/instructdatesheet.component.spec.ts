import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructdatesheetComponent } from './instructdatesheet.component';

describe('InstructdatesheetComponent', () => {
  let component: InstructdatesheetComponent;
  let fixture: ComponentFixture<InstructdatesheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructdatesheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructdatesheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
