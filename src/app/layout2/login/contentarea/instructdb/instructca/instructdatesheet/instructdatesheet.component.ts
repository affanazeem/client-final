import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
@Component({
  selector: 'app-instructdatesheet',
  templateUrl: './instructdatesheet.component.html',
  styleUrls: ['./instructdatesheet.component.css']
})
export class InstructdatesheetComponent implements OnInit {

  constructor(private router: Router) { }

  recent: boolean = true;
  old: boolean = true;
  date: any = ['a','b','c','e','f','g','h'];

  ngOnInit() {
      $(".call_modal_faculty").show(function () {
        $(".modal_faculty").fadeIn();
      });
      $(".closer_faculty").click(function () {
        $(".modal_faculty").fadeOut();
      });
  }

  home(){
    this.router.navigate(['/instructdb'])
  }
}
