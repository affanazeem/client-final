import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructcaComponent } from './instructca.component';

describe('InstructcaComponent', () => {
  let component: InstructcaComponent;
  let fixture: ComponentFixture<InstructcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
