import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructviewresultComponent } from './instructviewresult.component';

describe('InstructviewresultComponent', () => {
  let component: InstructviewresultComponent;
  let fixture: ComponentFixture<InstructviewresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructviewresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructviewresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
