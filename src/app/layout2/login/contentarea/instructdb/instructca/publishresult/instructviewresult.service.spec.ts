import { TestBed, inject } from '@angular/core/testing';

import { InstructviewresultService } from './instructviewresult.service';

describe('InstructviewresultService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstructviewresultService]
    });
  });

  it('should ...', inject([InstructviewresultService], (service: InstructviewresultService) => {
    expect(service).toBeTruthy();
  }));
});
