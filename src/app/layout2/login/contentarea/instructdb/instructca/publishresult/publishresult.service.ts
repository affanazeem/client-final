import { Injectable } from '@angular/core';
import {Headers, Http, Response} from "@angular/http";


@Injectable()
export class PublishresultService {

  constructor(private _http: Http) {
  }

  results: { firstname: '', lastname: '', classname: '', sectionname: '', subjectname: '', marks: '', grade: '' };

  allResults(addresult: any) {
    this.results = addresult;
    console.log('in all result method: ' + this.results.firstname + this.results.lastname + this.results.classname + this.results.sectionname + this.results.subjectname + this.results.grade + this.results.marks);
  }


  getAllStudents(classId: any, sectionname: any) {
    let url = 'http://localhost:3000/admins/StudentWithClassAndSections/' + classId + '/' + sectionname;
    return this._http.get(url).map((response: Response) => response.json());
  }

  postResults(allRecords: any) {

    console.log("ARRAY in service" + allRecords.firstname + allRecords.lastname + allRecords.classname + allRecords.sectionname
      + allRecords.subjectname + allRecords.marks + allRecords.grade);

    var body = {
      firstname: allRecords.firstname,
      lastname: allRecords.lastname,
      classname: allRecords.classname,
      sectionname: allRecords.sectionname,
      subjectname: allRecords.subjectname,
      grade: allRecords.grade,
      marks: allRecords.marks,
      approve:"false"
    };
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/results', body,
      {
        headers: headers,
      }).map(res => res.json())

  }
}
