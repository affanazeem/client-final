import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {TestingService} from "../../../../../../layout1/testing.service";
import {PublishresultService} from "./publishresult.service";
import {InstructdbService} from "../../instructdb.service";
import { Router } from '@angular/router';
@Component({
  selector: 'app-publishresult',
  templateUrl: './publishresult.component.html',
  styleUrls: ['./publishresult.component.css']
})
export class PublishresultComponent implements OnInit {

  classnameinvalid: boolean = false;
  classnamevalid: boolean = false;

  sectioninvalid: boolean = false;
  sectionvalid: boolean = false;

  subjectvalid: boolean = false;
  subjectinvalid: boolean = false;

  teachernamematch: boolean;
  teachernamevalid: boolean;
  teachernameinvalid: boolean;

  teachername: any;
  username: any;
  firstname: any;
  classname: any;
  subjectname: any;
  sectionname: any;
  error: boolean = false;

  input: any;

  allRecords: any = [];
  addresult = {firstname: '', lastname: '', classname: '', sectionname: '', subjectname: '', marks: '', totmarks:'', grade: ''};
  array: any = [];


  disable: boolean = false;
  array_results: any = [];

  constructor(private info: TestingService, private _httpService: PublishresultService, private _instructor: InstructdbService, private router: Router) {

    this.teachername = this.info.username;

    console.log(this.teachername);

    this.firstname = _instructor.firstname;
    console.log(this.firstname);

    this.classname = _instructor.classname;
    console.log(this.classname);
    this.addresult.classname = this.classname;

    this.sectionname = _instructor.sectionname;
    console.log(this.sectionname);
    this.addresult.sectionname = this.sectionname;

    this.subjectname = _instructor.subjectname;
    console.log(this.subjectname);
    this.addresult.subjectname = this.subjectname;
  }

  ngOnInit() {
    $(".modal_invalid").hide();
    $(".modal_marks").hide();
    this._httpService.getAllStudents(this.classname, this.sectionname).subscribe(
      data => {
        this.array = data;
      },
      error => {
        this.teachernamematch = true,
          this.teachernameinvalid = true,
          this.teachernamevalid = false
      },
      () => {
        this.teachernamematch = false,
          this.teachernamevalid = true ,
          this.teachernameinvalid = false
      }
    );
  }

  validateMarks(marks: any): boolean {

    if (marks != null && marks.match(/^[0-9]+$/) && marks.length <= 3) {
      this.addresult.marks = marks;

      console.log("Validate" + this.addresult.marks);

      var m = parseInt(marks);
      m = parseInt("" + this.addresult.marks, 0);

      if (m <= 50 && m >= 0) {
        this.addresult.grade = "F";
        console.log(this.addresult.grade);
        return true;
      }
      else if (m > 50 && m < 60) {
        this.addresult.grade = "D";
        console.log(this.addresult.grade);
        return true;
      }

      else if (m >= 60 && m < 70) {
        this.addresult.grade = "C";
        console.log(this.addresult.grade);
        return true;
      }

      else if (m >= 70 && m < 80) {
        this.addresult.grade = "B";
        console.log(this.addresult.grade);
        return true;
      }

      else if (m >= 80 && m < 90) {
        this.addresult.grade = "A-";
        console.log(this.addresult.grade);
        return true;
      }

      else if (m >= 90 && m <= 100) {
        this.addresult.grade = "A+";
        console.log(this.addresult.grade);
        return true;
      }

      else {
        alert('Please Re-Enter Marks!');
        this.addresult.grade = "";
        return false;
      }


    }

    else {
      $(".call_modal_invalid").show(function () {
        $(".modal_invalid").fadeIn();
        return false;
      });
    }
  }


  addres(v: any) {

    if (this.validateMarks(v.marks) == true) {
      for (var i = 0; i < this.array.length; i++) {
        if (v.id == this.array[i].id) {
          this.addresult.firstname = this.array[i].firstname;
          this.addresult.lastname = this.array[i].lastname;
        }
      }

      this._httpService.postResults(this.addresult).subscribe(
        data => {},

      error => {},

        () => { }
    )
    }

    else {
      $(".btn-warning").click(function () {
        $(".modal_invalid").fadeOut();
      });
    }
  }




  cancel() {

    $(".btn-warning").click(function () {
      $(".modal_invalid").fadeOut();
    });

  }


}
