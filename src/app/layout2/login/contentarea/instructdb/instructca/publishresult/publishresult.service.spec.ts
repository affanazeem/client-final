import { TestBed, inject } from '@angular/core/testing';

import { PublishresultService } from './publishresult.service';

describe('PublishresultService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PublishresultService]
    });
  });

  it('should ...', inject([PublishresultService], (service: PublishresultService) => {
    expect(service).toBeTruthy();
  }));
});
