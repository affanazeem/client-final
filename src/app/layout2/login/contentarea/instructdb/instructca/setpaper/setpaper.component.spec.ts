
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetpaperComponent } from './setpaper.component';

describe('SetpaperComponent', () => {
  let component: SetpaperComponent;
  let fixture: ComponentFixture<SetpaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetpaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetpaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
