import { Injectable } from '@angular/core';
import {Http, Headers, Response} from "@angular/http";

@Injectable()
export class SetpaperService {

  constructor(private _http: Http) { }



  addQuestion(tname: any, classname: any, sectionname:any, subjectname: any, content: any){

    console.log('in add ques ser');

    var body={tname:tname,classname:classname, sectionname:sectionname,subjectname:subjectname, content:content};

    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/teachers/addQuestion', body,
      {
        headers: headers
      }).map(res =>res.json())
  }

  getAllQuestions(tname,classname,sectionname,subjectname){

    let url = 'http://localhost:3000/teachers/getAllQuestions/' + tname + '/' + classname + '/' + sectionname + '/' + subjectname;
    return this._http.get(url).map((response: Response) => response.json());

}

  deleteQuestion(v:any){
      let url = 'http://localhost:3000/teachers/DeleteQuestion/'+v;
      return this._http.delete(url).map((response:Response) => response.json());
  }

  getQuestion(v: any){
    let url = 'http://localhost:3000/teachers/getQuestions/'+v;
    return this._http.get(url).map((response:Response) => response.json());

  }

  editQuestion(id:any,tname:any, classname:any, sectionname: any, subjectname:any, content: any){

    console.log('in edit api');
    var body={tname:tname, classname: classname,sectionname:sectionname, subjectname:subjectname, content:content};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/teachers/UpdateQuestion/'+id;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }

  getName()
  {
    let url = 'http://localhost:3000/teachers/getname';
    return this._http.get(url).map((response: Response) => response.json());
  }
}
