import { TestBed, inject } from '@angular/core/testing';

import { SetpaperService } from './setpaper.service';

describe('SetpaperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetpaperService]
    });
  });

  it('should ...', inject([SetpaperService], (service: SetpaperService) => {
    expect(service).toBeTruthy();
  }));
});
