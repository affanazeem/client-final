import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {InstructdbService} from "../../instructdb.service";
import {SetpaperService} from "./setpaper.service";

@Component({
  selector: 'app-setpaper',
  templateUrl: './setpaper.component.html',
  styleUrls: ['./setpaper.component.css']
})

export class SetpaperComponent implements OnInit {

  subjectname: any;
  firstname: any;
  classname: any;
  sectionname: any;
  content: string = '';
  id: number = 0;
  delid: any;

  array_ques : any = [];
  array :any = [];
  array_login: any = [];
  con: string='';
  editid : any;
  username: any;
  question: any = {firstname:'', classname:'', sectionname:'', subjectname:'', content:''};
  constructor(private _instruct: InstructdbService, private _httpService: SetpaperService) {

    this.firstname = _instruct.firstname;
    this.classname = _instruct.classname;
    this.sectionname = _instruct.sectionname;
    this.subjectname = _instruct.subjectname;

  }

  ngOnInit() {

    $(".modal_invalid").hide();
    $(".modal_update").hide();
    $('.modal_error').hide();
    this.question.firstname = this.firstname;
    this.question.subjectname = this.subjectname;
    this.question.sectionname = this.sectionname;
    this.question.firstname = this.subjectname;


    this._httpService.getName().subscribe(
      data => {

        this.array_login = data;
        this.username = this.array_login.username;
        this._httpService.getAllQuestions(this.firstname,this.classname,this.sectionname,this.subjectname).subscribe(

        data =>{this.array_ques = data},
        error => {},
        ()=>{}
      )

  })
  }


  exit(){
    $(".modal_update").hide();
  }

  add() {

    if(this.content == "") {

      $(".call_modal_invalid").show(function () {
        $(".modal_invalid").fadeIn();
      })
    }

    else {
      this.id++
      this._httpService.addQuestion(this.firstname, this.classname, this.sectionname, this.subjectname, this.content).subscribe(
        data => {},

        error => {
          $(".call_modal_error").show(function () {
          $(".modal_error").fadeIn();
        })
        },

        () => {}
      );
      this.question.content = this.content;
      this.content = '';
    }
  }

  cancel(){
    $(".modal_invalid").hide();
  }


  errorcancel(){
    $(".modal_error").hide();
  }



  delete(v: any){

    for(var i = 0 ; i < this.array_ques.length ; i++){
    if(v.id == this.array_ques[i].id){
      console.log('you clicked'+v.id);
      this.delid = v.id;
      $(".call_modal_approve").show(function () {
        $(".modal_approve").fadeIn();
      });
      $(".closer_approve").click(function () {
        $(".modal_approve").fadeOut();
      });
      }
    }
  }


  yesApprove(){

    $(".btn-blue").click(function () {
      $(".modal_approve").hide();
    });

    for(var i = 0 ; i < this.array_ques.length ; i++){
      if(this.delid == this.array_ques[i].id){
        this.array_ques.splice(i, 1);
      }
    }

    this._httpService.deleteQuestion(this.delid).subscribe(

      data => {},

      error => {},

      () =>{}

    )

    this.content= '';

    $(".modal_approve").hide();
  }


  noApprove(){
    $(".modal_approve").hide();
  }

  edit(v: any){

    $(".call_modal_update").show(function () {
      $(".modal_update").fadeIn();
    });
    for(var i = 0 ; i < this.array_ques.length; i++) {
      if (v.id == this.array_ques[i].id) {
        this.editid = v.id;
        this._httpService.getQuestion(v.id).subscribe(
          data => {
            this.array = data;
            this.con = this.array.content;
        });
      }
   }
}

  update()
  {

      if(this.con == ""){
      $('.modal_update').hide();
      $(".call_modal_invalid").show(function () {
        $(".modal_invalid").fadeIn();

      });
    }

    else {
      $(".modal_update").hide();
      $(".call_modal_edit").show(function () {
        $(".modal_edit").fadeIn();
      });
    }

  }

  yesEdit(){

    this._httpService.editQuestion(this.editid, this.firstname, this.classname, this.sectionname, this.subjectname, this.con).subscribe(

      data => {},

      error => {},

      () =>{}

    )

    this.content= '';
    $(".modal_edit").hide();

    }




  noEdit(){
    $(".modal_edit").hide();
  }


}
