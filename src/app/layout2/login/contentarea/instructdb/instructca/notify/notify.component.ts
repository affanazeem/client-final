import { Component, OnInit } from '@angular/core';
import {NotifyService} from "./notify.service";
import {TestingService} from "../../../../../../layout1/testing.service";


@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.css']
})
export class NotifyComponent implements OnInit {

  username: any;
  array: any = [];
  array_notification: any = [];
  teacher: any = {firstname: '', lastname: ''};
  post: any = "teacher";
  array_exam: any = [];
  array_announce: any = [];

  array_login: any = [];

  constructor(private _httpService: NotifyService, private _testing: TestingService) {

    this.username = _testing.username;
    console.log("username" + this.username);

  }

  ngOnInit() {

    this._httpService.getName().subscribe(
      data => {

        this.array_login = data;
        this.username = this.array_login.username;

        this._httpService.getFullRecord(this.username).subscribe(
          data => {
            this.array = data;
            console.log("Array" + this.array.firstname);
            this.teacher.firstname = this.array.firstname;
            this.teacher.lastname = this.array.lastname;

            this._httpService.getNotification(this.teacher.firstname).subscribe(
              data => {
                this.array_exam = data;
              },
              error => {
              },
              () => {
              }
            );
          },
        );

        this._httpService.getTeacherAnnouncement(this.post).subscribe(
          data => {
            this.array_announce = data
          },
          error => {
          },
          () => {
          },
        );
      },
      error => {
      },
      () => {
      }
    )


  }
}

