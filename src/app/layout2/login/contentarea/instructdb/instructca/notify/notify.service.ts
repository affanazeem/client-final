import { Injectable } from '@angular/core';
import {Http, Response, Headers} from "@angular/http";

@Injectable()
export class NotifyService {

  constructor(private _http: Http) { }

  getFullRecord(username: any){
    console.log('in full recoed');
    let url = 'http://localhost:3000/admins/FullRecordOfTeacher/'+username;
    return this._http.get(url).map((response:Response) => response.json());
  }

  getNotification(firstname: any){
    console.log("first name in serv"+ firstname);
    let url = 'http://localhost:3000/teachers/NotificationOfTeacher/'+firstname;
    return this._http.get(url).map((response:Response) => response.json());
  }
  //
  //
  // notifications(firstname: any, lastname: any){
  //   console.log("first name in serv"+ firstname+lastname);
  //   let url = 'http://localhost:3000/teachers/returnNotification/'+firstname+'/'+lastname;
  //   return this._http.get(url).map((response:Response) => response.json());
  // }

  getTeacherAnnouncement(sendto){
    let url = 'http://localhost:3000/admins/returnAnnouncement/'+sendto;
    return this._http.get(url).map((response:Response) => response.json());
  }

  getName()
  {
    let url = 'http://localhost:3000/teachers/getname';
    return this._http.get(url).map((response: Response) => response.json());
  }


}
