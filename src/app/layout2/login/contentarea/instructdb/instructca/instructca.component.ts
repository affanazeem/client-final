import { Component, OnInit } from '@angular/core';

import * as $ from 'jquery';

@Component({
  selector: 'app-instructca',
  templateUrl: './instructca.component.html',
  styleUrls: ['./instructca.component.css']
})
export class InstructcaComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $(".login-html").fadeOut();
    $(".login-html").hide();
    $(".main").fadeOut();
    $(".main").hide();
  }
}
