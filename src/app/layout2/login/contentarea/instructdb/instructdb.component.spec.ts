import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructdbComponent } from './instructdb.component';

describe('InstructdbComponent', () => {
  let component: InstructdbComponent;
  let fixture: ComponentFixture<InstructdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
