import { Component, OnInit } from '@angular/core';
import {AdmindbService} from "./admindb.service";
import {PublishresultService} from "../instructdb/instructca/publishresult/publishresult.service";
import { Router } from '@angular/router';
@Component({
  selector: 'app-admindb',
  templateUrl: './admindb.component.html',
  styleUrls: ['./admindb.component.css']
})
export class AdmindbComponent implements OnInit {


  array: any = [];

  array_teachers: any = [];
  array_students: any = [];
  constructor(private router:Router, private _adminService : AdmindbService, private _publishResultService: PublishresultService) {
  }

  ngOnInit() {


  }

  results(){
    console.log('at view ');
    this.router.navigate(['admindb/adminca/viewresults']);
  }


  result() {
    this._adminService.getAllStudents().subscribe(
      data =>
      {
        this.array = data;
        console.log(this.array)
        },
      error => {
      },
      () => {}
    );
  }

  datesheet(){
    console.log('in ds');
    this.router.navigate(['/admindb/adminca/datesheet'])
  }

}
