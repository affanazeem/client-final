import { TestBed, inject } from '@angular/core/testing';

import { AssignclasstoteacherService } from './assignclasstoteacher.service';

describe('AssignclasstoteacherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssignclasstoteacherService]
    });
  });

  it('should ...', inject([AssignclasstoteacherService], (service: AssignclasstoteacherService) => {
    expect(service).toBeTruthy();
  }));
});
