import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

@Injectable()
export class AssignclasstoteacherService {

  constructor(private _http:Http) { }

  postCheckSubjectName(addteacher: any){

    console.log('okkk');
    console.log("subject name" +addteacher.csubjectname);
    var body = {subjectname:addteacher.subjectname, classname:addteacher.classname};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/returnsubjectname', body,
      {
        headers: headers,
      }).map(res => res.json())
  }

  postCheckClassName(addcourse: any){

    console.log('okkk');
    console.log("Course no" +addcourse.classname);
    var body = {classname:addcourse.classname};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/returnclassname', body,
      {
        headers: headers,
      }).map(res => res.json())
  }



  getDuplicate(addteacher:any){

    let url = 'http://localhost:3000/admins/Duplicate/'+addteacher.firstname+'/'+addteacher.classname+'/'+addteacher.sectionname+'/'+addteacher.subjectname;
    return this._http.get(url).map((response:Response) => response.json());
  }

  getFirstName(addteacher:any){

    let url = 'http://localhost:3000/admins/teacherExist/'+addteacher.firstname;
    return this._http.get(url).map((response:Response) => response.json());
  }

  postCheckSectionAndClass(addteacher:any){
    let url = 'http://localhost:3000/admins/ClassWithSection/'+addteacher.classname+'/'+addteacher.sectionname;
    return this._http.get(url).map((response:Response) => response.json());
  }


  postAssignClass(addteacher){

    console.log(addteacher.firstname+addteacher.classname+addteacher.sectionname+addteacher.subjectname);


    var body={firstname:addteacher.firstname, classname:addteacher.classname,sectionname:addteacher.sectionname,subjectname:addteacher.subjectname};
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    console.log('yeahan agaya!!!!!!!!!!!!!!');
    return this._http.post('http://localhost:3000/admins/teacherClass', body,
      {
        headers: headers
      }).map(res =>res.json())
  }


}

