import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {AssignclasstoteacherService} from "./assignclasstoteacher.service";
@Component({
  selector: 'app-assign-class-to-teacher',
  templateUrl: './assign-class-to-teacher.component.html',
  styleUrls: ['./assign-class-to-teacher.component.css']
})
export class AssignClassToTeacherComponent implements OnInit {

  addteacher: any = {firstname:'',classname:'',sectionname:'', subjectname:''};
  array: any = [];
  array_subject =[];

  dup:boolean;
  dupexist:boolean;
  i:number;
  classnameexist: boolean;

  firstnomatch: boolean = false;
  firstnamematch: boolean = false;

  generalmatch: boolean = false;

  firstnameinvalid: boolean;
  firstnamevalid: boolean;

  classnamevalid: boolean;
  classnameinvalid: boolean;

  sectionnameinvalid: boolean;
  sectionnamevalid: boolean;

  subjectnameinvalid: boolean;
  subjectnamevalid: boolean;

  firstnoexist : boolean;
  firstNameexist : boolean;
  sectionNameexist : boolean = false;
  subjectNameexist : boolean = false;
  success : boolean;

  flag: boolean= false;
  constructor(private _httpService: AssignclasstoteacherService) { }

  ngOnInit() {
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
  }

  validateclassname():boolean {
   if(this.addteacher.classname != null && this.addteacher.classname.match(/^[1-9]+$/) && this.addteacher.classname.length <=2 && this.classnameexists()){
       // console.log('validation through validate class name');
         this.classnameinvalid = false;
         this.classnamevalid = true;
         this.success = false;
         return true;
       }

    else {
      this.classnameinvalid = true;
      this.classnamevalid = false;
      this.success = true;
      return false;
    }
  }


  validateSubjectname():boolean {
    if(this.addteacher.subjectname != null && this.addteacher.subjectname.match(/^[A-Z a-z]+$/) && this.subjectnameexists()){
      console.log('validation through validate subject name');
      this.subjectnameinvalid = false;
      this.subjectnamevalid = true;
      this.success = false;
      return true;
    }

    else {
      this.subjectnameinvalid = true;
      this.subjectnamevalid = false;
      this.success = true;
      return false;
    }
  }

  validatefirstname():boolean {

    if(this.addteacher.firstname != null && this.addteacher.firstname.match(/^[A-Z a-z]+$/) &&
      this.firstnameexist() == true){
      this.firstnameinvalid = false;
      this.success = false;
      this.firstnamevalid = true;
      return true;
    }

    else {
      this.firstnameinvalid = true;
      this.firstnamevalid = false;
      this.success = true;
      return false;
    }
  }


  classnameexists() : boolean {
    // console.log('yeahan agae!');
    this._httpService.postCheckClassName(this.addteacher).subscribe(
      data =>
      error => {
        this.classnameexist = true,
        this.classnameinvalid=true,
        this.classnamevalid = false,
        this.success = true;
      },
      () => {
        this.classnameexist = false,
          this.classnamevalid=true ,
          this.classnameinvalid = false
        this.success = false;
      }
    );

    if(this.classnameexist == true){
      return false;

    }
    else{
      // console.log('chal gaya!');
      return true;
    }
  }


  subjectnameexists() : boolean {
    console.log('yeahan agae!');
    this._httpService.postCheckSubjectName(this.addteacher).subscribe(
      data => {
       this.array_subject = data
      console.log("length of subjects::"+this.array_subject.length)
        if(this.array_subject.length == 0){
        console.log('subject is not assigned for this class')
      }
      else {
          for (var i = 0; i < this.array_subject.length; i++) {
            if (this.array_subject[i].coursename == this.addteacher.subjectname) {
              console.log("section from array: " + this.array_subject[this.i].subjectname)
              this.subjectnamevalid = true;
              this.subjectnameinvalid = false;
              this.success = false;
              break;
            }
            else {
              this.subjectnameinvalid = true;
              this.subjectnamevalid = false;
              this.success = true;
            }
          }
        }
      },
      error => {
        this.subjectNameexist = true,
          this.subjectnameinvalid=true,
          this.subjectnamevalid = false,
          this.success = true;
      },
      () => {
        this.subjectNameexist = false,
          this.subjectnamevalid=true ,
          this.subjectnameinvalid = false
        this.success = false;
      }
    );

    if(this.subjectNameexist == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }
  }




  sectionnameexists(): boolean{
    // console.log('yeahan agae!');
    this._httpService.postCheckSectionAndClass(this.addteacher).subscribe(
      data => {
        this.array = data

        for (this.i = 0; this.i < this.array.length; this.i++) {
        if(this.addteacher.sectionname == this.array[this.i].sectionname) {
         // console.log("section from array: "+this.array[this.i].sectionname)
          this.sectionnamevalid = true;
          this.sectionnameinvalid = false;
          this.success = false;
          break;
        }
        else{
          this.sectionnameinvalid = true;
          this.sectionnamevalid = false;
          this.success = true;
        }
      }
      },

      error => {
        this.sectionNameexist = false,

        this.sectionnameinvalid=true,
        this.sectionnamevalid = false,

        this.success = true;
        return false;
      },

      ()=>console.log('ok'));

    if(this.sectionnameinvalid == true){
      return false;
    }
    else{
      return true;
    }
      }


  validatesectionname():boolean {
    if(this.addteacher.sectionname != null && this.addteacher.sectionname.match(/^[A-D]+$/)) {
      if (this.sectionnameexists()) {
        this.sectionnameinvalid = false;
        this.sectionnamevalid = true;
        this.success = false;
        return true;
      }
      else {

        this.sectionnameinvalid = true;
        this.sectionnamevalid = false;

        return false;
      }
    }
    else
    {
      this.sectionnameinvalid = true;
      this.sectionnamevalid = false;

      this.success = true;
      return false;
    }

  }



  firstnameexist() :boolean {
    // console.log('yeahan agae!');
    this._httpService.getFirstName(this.addteacher).subscribe(
      data => console.log(),
      error => {
        this.firstNameexist = true,
        this.firstnameinvalid = true,
        this.firstnamevalid = false
        this.success = true;
      },
      () => {
        this.firstNameexist = false,
          this.firstnamevalid=true,
          this.firstnameinvalid = false,
          this.success =false;
      }
    );

    if(this.firstNameexist == true){
      return false;

    }
    else{
      // console.log('chal gaya!');
      return true;
    }

  }

  duplicate(){

    // console.log('yeahan agae!');
    this._httpService.getDuplicate(this.addteacher).subscribe(
      data => console.log('data value: ' + data.firstnamematch),
      error => {
      // console.log('error recieved!'),
        this.dup = true,
         this.dupexist=true,
        this.success = true
      },
      () => {
          this.dup = false,
            this.dupexist=false,
          this.success =false;
      }
    );
    if(this.dupexist == true){
      // console.log("dupliate mil gaya!");
      // return false;

    }
    else{
      // console.log('duplicate nhi mila!');
      // return true;
    }


  }

  Success(){

    $(".call_modal").show(function(){
      $(".modal").fadeIn();

    });

    $(document).ready(function(){
      $(".closer").click(function(){
        $(".modal").fadeOut();
      });
    });

  }


  AssignTeacher() {

    if (this.addteacher.firstname != null && this.addteacher.firstname.match(/^[A-Z a-z]+$/)
      && this.addteacher.classname != null && this.addteacher.classname.match(/^[0-9]+$/) &&
      this.validateclassname() == true && this.validatesectionname() == true  && this.validateSubjectname()) {
        console.log('if the andar hai!');
        this._httpService.postAssignClass(this.addteacher).subscribe(
          data => console.log(),
          error => {
          },

          () => {
            this.addteacher.firstname = null;
            this.addteacher.classname = null;
            this.addteacher.sectionname = null;

            this.classnamevalid = false;
            this.classnameinvalid = false;

            this.sectionnamevalid = false;
            this.sectionnameinvalid = false;

            this.firstnamevalid = false;
            this.firstnameinvalid = false;

            this.subjectnamevalid = false;
            this.subjectnameinvalid = false;

            this.success = false;

            this.Success();

          }
        );

      }

  else {
      this.subjectnameinvalid= true,
      this.sectionnameinvalid= true,
        this.classnameinvalid= true,
        this.firstnameinvalid = true,
        this.generalmatch = true;
      this.success = true;
      // console.log('not added!');
    }
  }


  clear(){
    this.addteacher.firstname = null;
    this.addteacher.classname = null;
    this.addteacher.sectionname = null;
    this.addteacher.subjectname = null;

    this.classnamevalid = false;
    this.classnameinvalid = false;

    this.sectionnamevalid = false;
    this.sectionnameinvalid = false;


    this.subjectnamevalid = false;
    this.subjectnameinvalid = false;


    this.firstnamevalid = false;
    this.firstnameinvalid = false;

    this.success = false;
  }


}

