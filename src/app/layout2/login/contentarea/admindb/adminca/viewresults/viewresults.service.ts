import { Injectable } from '@angular/core';

import {Headers, Http, Response} from "@angular/http";


@Injectable()
export class ViewresultsService {

  constructor(private _http:Http) { }

  classname:any;
  sectionname:any;
  subjectname:any;

  getAllResults(){
    let url = 'http://localhost:3000/admins/ResultOfClasses/';
    return this._http.get(url).map((response:Response) => response.json());
  }

  Classes(classname: any, sectionname: any, subjectname: any) {
    this.classname = classname;
    this.sectionname = sectionname;
    this.subjectname = subjectname;
  }




}
