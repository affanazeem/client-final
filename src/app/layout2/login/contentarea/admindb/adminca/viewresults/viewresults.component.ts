import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AdmindbService} from "../../admindb.service";
import {ViewresultsService} from "./viewresults.service";

@Component({
  selector: 'app-viewresults',
  templateUrl: './viewresults.component.html',
  styleUrls: ['./viewresults.component.css']
})
export class ViewresultsComponent implements OnInit {

  constructor(private router:Router, private _adminService : AdmindbService, private _httpService: ViewresultsService) { }

  array :any = [];

  ngOnInit() {

    this._httpService.getAllResults().subscribe(

      data => {

        this.array = data;
        console.log(this.array);
      },
      error => {},
      () => {}

    )
}

  see(v:any){

    for(var i = 0 ; i < this.array.length ; i++){
      if(v.id == this.array[i].id){
        this._httpService.Classes(this.array[i].classname, this.array[i].sectionname, this.array[i].subjectname);
        this.router.navigate(['/admindb/adminca/approveresults']);
      }
    }

  }

}
