import { TestBed, inject } from '@angular/core/testing';

import { ViewresultsService } from './viewresults.service';

describe('ViewresultsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewresultsService]
    });
  });

  it('should ...', inject([ViewresultsService], (service: ViewresultsService) => {
    expect(service).toBeTruthy();
  }));
});
