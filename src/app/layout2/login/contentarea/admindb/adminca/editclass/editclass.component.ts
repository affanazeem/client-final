import { Component, OnInit } from '@angular/core';
import {EditclassserviceService} from "./editclassservice.service";

@Component({
  selector: 'app-editclass',
  templateUrl: './editclass.component.html',
  styleUrls: ['./editclass.component.css']
})
export class EditclassComponent implements OnInit {

  viewclass: any = {id: '', classname: ''};
  array: any = [];

  constructor(private _httpService: EditclassserviceService) {
  }

  ngOnInit() {
    console.log('here!');
    this._httpService.getAllClasses().subscribe(
      data => {
        console.log(data),
          this.viewclass = data.json(),
          this.array.push(this.viewclass),
          console.log(this.array.length)
      },
      error => console.log('error'),
      () => console.log('yahoo')
    )

  }

}
