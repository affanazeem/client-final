import { TestBed, inject } from '@angular/core/testing';

import { EditclassserviceService } from './editclassservice.service';

describe('EditclassserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditclassserviceService]
    });
  });

  it('should ...', inject([EditclassserviceService], (service: EditclassserviceService) => {
    expect(service).toBeTruthy();
  }));
});
