import { Injectable } from '@angular/core';
import {Headers, Http, Response} from "@angular/http";


@Injectable()
export class ApproveresultsService {

  constructor(private _http:Http) { }

  getResults(classname:any,sectionname:any,subjectname:any){
    let url = 'http://localhost:3000/admins/getAllResults/'+classname+'/'+sectionname+'/'+subjectname;
    return this._http.get(url).map((response:Response) => response.json());
  }



  DeleteRecord(v:number){
    console.log('in delete service');
    let url = 'http://localhost:3000/admins/DeleteResult/'+v;
    return this._http.delete(url).map((response:Response) => response.json());
  }


  updateRecord(v:any){


    var body={firstname:v.firstname,lastname:v.lastname,classname:v.classname,sectionname:v.sectionname,
    subjectname:v.subjectname,marks:v.marks,grade:v.grade, approve:"false"};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/UpdateResult/'+v.firstname+'/'+v.lastname+'/'+v.classname+'/'+v.sectionname+'/'+v.subjectname;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }

  updateApprove(v: any){


    var body={firstname:v.firstname,lastname:v.lastname,classname:v.classname,sectionname:v.sectionname,
      subjectname:v.subjectname,marks:v.marks,grade:v.grade,approve:"true"};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/Approve/'+v.firstname+'/'+v.lastname+'/'+v.classname+'/'+v.sectionname+'/'+v.subjectname;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }

}
