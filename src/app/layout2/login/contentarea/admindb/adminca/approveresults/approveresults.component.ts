import { Component, OnInit } from '@angular/core';
import {ViewresultsService} from "../viewresults/viewresults.service";
import {ApproveresultsService} from "./approveresults.service";
import * as $ from 'jquery';
import {validate} from "codelyzer/walkerFactory/walkerFn";

@Component({
  selector: 'app-approveresults',
  templateUrl: './approveresults.component.html',
  styleUrls: ['./approveresults.component.css']
})
export class ApproveresultsComponent implements OnInit {


  classname: any;
  sectionname: any;
  subjectname: any;
  isEdit: boolean = false;

  a: any = {firstname: '', lastname: '', classname: '', grade: '', marks: ''}
  array: any = [];

  editres: any = {firstname: '', lastname: '', classname:'',sectionname:'',subjectname:'',marks: '',grade:''};
  Noeditres: any = {firstname: '', lastname: '', classname:'',sectionname:'',subjectname:'',marks: '',grade:''};
  array_student: any = [];

  constructor(private _viewResults: ViewresultsService, private _httpService: ApproveresultsService) {

    this.classname = this._viewResults.classname;
    this.sectionname = this._viewResults.sectionname;
    this.subjectname = this._viewResults.subjectname;

    console.log("in approve!");
    console.log(this.classname);
    console.log(this.sectionname);
    console.log(this.subjectname);

  }


  ngOnInit() {

    this._httpService.getResults(this.classname, this.sectionname, this.subjectname).subscribe(
      data => {
        this.array = data
      },
    )
  }



  validateGrade(marks:any, grade:any):boolean{

    if(this.validateMarks(marks) == true){
      return true;
    }

    else
    {
      return false;
    }

  }


  validateMarks(marks: any):boolean{
    console.log('at validate marks'+marks);

    if(marks!= null && marks.match(/^[0-9]+$/) && marks.length<=3) {


      var m = parseInt(marks);
      m = parseInt("" + marks, 0);

      if (m <= 50 && m >= 0) {
        this.editres.grade = "F";
        console.log(this.editres.grade);
        return true;
      }
      else if (m > 50 && m < 60) {
        this.editres.grade = "C";
        console.log(this.editres.grade);
        return true;
      }

      else if (m >= 60 && m < 70) {
        this.editres.grade = "C";
        console.log(this.editres.grade);
        return true;
      }

      else if (m >= 70 && m < 80) {
        this.editres.grade = "B";
        console.log(this.editres.grade);
        return true;
      }

      else if (m >= 80 && m < 90) {
        this.editres.grade = "A-";
        console.log(this.editres.grade);
        return true;
      }

      else if (m >= 90 && m <= 100) {
        this.editres.grade = "A+";
        console.log(this.editres.grade);
        return true;
      }

      else {
        alert('Please Re-Enter Marks!');
        this.editres.grade = "";
        return false;
      }

    }
      else {
        alert('Please Re-Enter Marks!');
        this.editres.grade = "";
        return false;
      }
  }



  edit(v)
  {
    console.log('clicked!');
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });

    this.editres.firstname = v.firstname;
    this.editres.lastname = v.lastname;
    this.editres.classname = v.classname;
    this.editres.sectionname = v.sectionname;
    this.editres.subjectname = v.subjectname;

  }



  no(){
    this.isEdit = false;
    $(".btn-red").click(function () {
      console.log('no!');
      $(".modal_delete").fadeOut();
    });
  }

  yes(){


    this._httpService.updateRecord(this.editres).subscribe(
      data => this.array = data,
      error => console.log('error!'),
    )

    $(".btn-blue").click(function () {
      $(".modal_delete").fadeOut();
    });
    this.isEdit = true;
  }

  approve(v: any){

    this.Noeditres.firstname = v.firstname;
    this.Noeditres.lastname = v.lastname;
    this.Noeditres.classname = this.classname;
    this.Noeditres.sectionname = this.sectionname;
    this.Noeditres.subjectname = this.subjectname;
    this.Noeditres.marks = v.marks;
    this.Noeditres.grade = v.grade;

    $(".call_modal_approve").show(function () {
      $(".modal_approve").fadeIn();
    });
    $(".closer_approve").click(function () {
      $(".modal_approve").fadeOut();
    });

  }



  yesApprove(){


    console.log("firstname"+this.Noeditres.firstname);
    console.log("lastname"+this.Noeditres.lastname);
    console.log("classname"+this.classname);
    console.log("sectionname"+this.sectionname);
    console.log("subjectname"+this.subjectname);
    console.log("marks"+this.Noeditres.marks);
    console.log("grade"+this.Noeditres.grade);



    if(this.isEdit == false){
    console.log("is edit is false!");
      this._httpService.updateApprove(this.Noeditres).subscribe(
        data => this.array = data,
        error => console.log('error!'),
      )

      $(".btn-blue").click(function () {
        $(".modal_approve").fadeOut();
      });

    }

    else {
      this._httpService.updateApprove(this.editres).subscribe(
        data => this.array = data,
        error => console.log('error!'),
      )

      $(".btn-blue").click(function () {
        $(".modal_approve").fadeOut();
      });
    }
  }

  noApprove(){
    $(".btn-red").click(function () {
      console.log('no!');
      $(".modal_approve").fadeOut();
    });
  }




  delete(v: any) {
    console.log('outside for');
    for (var i = 0; i < this.array.length; i++) {
      console.log('in for');
      if (v.id == this.array[i].id) {
        this.array.splice(i, 1);
        this._httpService.DeleteRecord(v.id).subscribe(
          data => {
            // this.array = data
          },
          error => {
            console.log('error')
          },
          () => {
            console.log('complete')
          }
        )
      }
    }
  }
}

