import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveresultsComponent } from './approveresults.component';

describe('ApproveresultsComponent', () => {
  let component: ApproveresultsComponent;
  let fixture: ComponentFixture<ApproveresultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveresultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveresultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
