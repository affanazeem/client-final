import { TestBed, inject } from '@angular/core/testing';

import { ApproveresultsService } from './approveresults.service';

describe('ApproveresultsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApproveresultsService]
    });
  });

  it('should ...', inject([ApproveresultsService], (service: ApproveresultsService) => {
    expect(service).toBeTruthy();
  }));
});
