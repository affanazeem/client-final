import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";

@Injectable()
export class InvigilatorService {

  constructor(private _http:Http) { }

  examId:any;
  classname:any;
  tname:any;
  dateexam:any;
  time:any;
  subjectname:any;

  getAllExams(){
    let url = 'http://localhost:3000/admins/AllExams/';
    return this._http.get(url).map((response:Response) => response.json());
  }

  setinv(examid: any,dateexam: any, time: any, tname: any,subjectname: any,classname: any,examId:any) {

    this.classname = classname;
    this.tname = tname;
    this.subjectname = subjectname;
    this.dateexam = dateexam;
    this.time = time;
    this.examId = examid;
  }


}
