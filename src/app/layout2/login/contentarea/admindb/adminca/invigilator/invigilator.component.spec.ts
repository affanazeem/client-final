import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvigilatorComponent } from './invigilator.component';

describe('InvigilatorComponent', () => {
  let component: InvigilatorComponent;
  let fixture: ComponentFixture<InvigilatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvigilatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvigilatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
