import { Component, OnInit } from '@angular/core';
import {InvigilatorService} from "./invigilator.service";
import {Router} from "@angular/router";
import * as $ from 'jquery';
@Component({
  selector: 'app-invigilator',
  templateUrl: './invigilator.component.html',
  styleUrls: ['./invigilator.component.css']
})
export class InvigilatorComponent implements OnInit {


  array:any = [];

  constructor(private router:Router,  private _httpService: InvigilatorService) { }

  ngOnInit() {

    $(".modal_invalid").hide();
    this._httpService.getAllExams().subscribe(

      data => {

        this.array = data;
        console.log(this.array);
      },
      error => {
        $(".call_modal_invalid").show(function () {
        $(".modal_invalid").fadeIn();

      })},
      () => {}

    )
  }


  invigilate(v:any){

    for(var i = 0 ; i < this.array.length ; i++){
      if(v.id == this.array[i].id){
        this._httpService.setinv(this.array[i].id,this.array[i].dateexam, this.array[i].time,this.array[i].tname,this.array[i].subjectname, this.array[i].classname, this.array[i].id);
        this.router.navigate(['/admindb/adminca/setinv']);
      }
    }

  }
  ok(){
    $(".btn-warning").click(function () {
      $(".modal_invalid").fadeOut();
    });

  }


}
