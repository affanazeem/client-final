import { Injectable } from '@angular/core';
import {Http, Headers} from "@angular/http";

@Injectable()
export class ClasswallserviceService {

  constructor(private _http: Http) {
  }


  postCheckClassAndSectionExist(addclass: any) {
    console.log('okkk');
    console.log("Course name" + addclass.classname);
    var body = {classname: addclass.classname};
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/addClass', body,
      {
        headers: headers
      }).map(res => res.json())
  }


  postAddAssignClass(assignclass: any) {
    console.log('okkk');
    console.log("Class name" + assignclass.classname + "Section name" + assignclass.sectionname);
    var body = {classname: assignclass.classname, sectionname: assignclass.sectionname};
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/assignClass', body,
      {
        headers: headers
      }).map(res => res.json())
  }
}
