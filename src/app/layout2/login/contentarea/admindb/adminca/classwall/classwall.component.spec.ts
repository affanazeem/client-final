import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasswallComponent } from './classwall.component';

describe('ClasswallComponent', () => {
  let component: ClasswallComponent;
  let fixture: ComponentFixture<ClasswallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasswallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClasswallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
