import { Component, OnInit } from '@angular/core';
import {ClasswallserviceService} from "./classwallservice.service";
import * as $ from 'jquery'
@Component({
  selector: 'app-classwall',
  templateUrl: './classwall.component.html',
  styleUrls: ['./classwall.component.css']
})
export class ClasswallComponent implements OnInit {

  wall:any = {classname:'',sectionname:''};
  showmodal: boolean = false;



  constructor(private _httpService: ClasswallserviceService) {

    this.showmodal = false;
  }

  ngOnInit() {

    $(".lighter").fadeIn();
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();

    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
  }

  wallclassnameexist: boolean;

  wallnamematch: boolean = false;

  wallclassnameinvalid: boolean;
  wallclassnamevalid: boolean;


  wallsectionnameinvalid: boolean;
  wallsectionnamevalid: boolean;

  wallclassnameexists: boolean;

  wallclassNameexist : boolean;
    wallsuccess: boolean;


  // classexist():boolean{
  //   console.log('yeahan agae!');
  //   this._httpService.postCheckClassAndSectionExist(this.wall).subscribe(
  //     data => console.log('data value: ' +data.classnameinvalid),
  //     error => {
  //       this.wallsuccess = true,
  //         this.wallclassnameexist = true,
  //         this.wallclassnameinvalid=true,
  //         this.wallclassnamevalid = false
  //     },
  //     () => {
  //       this.wallsuccess = false,
  //         this.wallclassnameexist = false,
  //         this.wallclassnamevalid=true,
  //         this.wallclassnameinvalid = false
  //     }
  //
  //   );
  //
  //   if(this.wallclassnameexist == true){
  //     return false;
  //
  //   }
  //   else{
  //     console.log('chal gaya!');
  //     return true;
  //   }
  // }



  validatewallclassname():boolean {
    console.log('ok');

    if(this.wall.classname != null && this.wall.classname.match(/^[0-9]+$/)){
      this.wallclassnameinvalid = false;
      this.wallclassnamevalid = true;
      this.wallsuccess = false;
      return true;
    }

    else {
      this.wallclassnameinvalid = true;
      this.wallclassnamevalid = false;
      this.wallsuccess = true;
      return false;
    }
  }


  validatewallsectionname():boolean {
    console.log('ok');

    if(this.wall.sectionname != null && this.wall.sectionname.match(/^[A-D]+$/) && this.wall.sectionname.length ==1){
      this.wallsectionnameinvalid = false;
      this.wallsectionnamevalid = true;
      this.wallsuccess = false;
      return true;
    }

    else {
      this.wallsectionnameinvalid = true;
      this.wallsectionnamevalid = false;
      this.wallsuccess = true;
      return false;
    }
  }


  wallClass() {
    if (this.wall.classname != null && this.wall.sectionname.match(/^[0-9]+$/)
        // .&& this.validatewallclassname() == true
      && this.validatewallsectionname() == true) {

      console.log('here!');
      this._httpService.postAddAssignClass(this.wall).subscribe(
        data =>  console.log('data value: ' +data.classnamematch),
        error => {
          // this.wallsuccess = true
        },
        () => {

          this.wallclassnamevalid = true,
          this.wallclassnameinvalid = false,
          this.wallsuccess = false;
          this.showmodal =  false;
          console.log('success!')
          this.showmodal = true;
        }
      );

    }
    else {
      this.wallsuccess = true;
      console.log('not assigned!');
      // $(".closer").click(function () {
        $(".modal").fadeOut();
        this.showmodal = true;
      // });
 }
  }
}
