import { TestBed, inject } from '@angular/core/testing';

import { ClasswallserviceService } from './classwallservice.service';

describe('ClasswallserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClasswallserviceService]
    });
  });

  it('should ...', inject([ClasswallserviceService], (service: ClasswallserviceService) => {
    expect(service).toBeTruthy();
  }));
});
