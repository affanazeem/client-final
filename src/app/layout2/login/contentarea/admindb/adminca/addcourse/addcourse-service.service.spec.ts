import { TestBed, inject } from '@angular/core/testing';

import { AddcourseServiceService } from './addcourse-service.service';

describe('AddcourseServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddcourseServiceService]
    });
  });

  it('should ...', inject([AddcourseServiceService], (service: AddcourseServiceService) => {
    expect(service).toBeTruthy();
  }));
});
