import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import * as $ from 'jquery';
import {AddcourseServiceService} from "./addcourse-service.service";
@Component({
  selector: 'app-addcourse',
  templateUrl: './addcourse.component.html',
  styleUrls: ['./addcourse.component.css']
})
export class AddcourseComponent implements OnInit {

  addcourse: any = {coursename:'',classname:''};

  classnameexist: boolean;

  coursenomatch: boolean = false;
  coursenamematch: boolean = false;
  generalmatch: boolean = false;

  classnamevalid: boolean;
  classnameinvalid: boolean;

  coursenameinvalid: boolean;
  coursenamevalid: boolean;

  coursenoexist : boolean;
  courseNameexist : boolean;
  success : boolean;

  constructor(private _httpService: AddcourseServiceService) { }

  ngOnInit() {
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
  }


  validateclassname():boolean {

    if(this.addcourse.classname != null && this.addcourse.classname.match(/^[0-9]+$/)&&
      this.classnameexists() ){
      this.classnameinvalid = false;
      this.classnamevalid = true;
      this.success = false;
      return true;
    }

    else {
      this.classnameinvalid = true;
      this.classnamevalid = false;
      this.success = true;
      return false;
    }
  }

  validatecoursename():boolean {

    if(this.addcourse.coursename != null && this.addcourse.coursename.match(/^[A-Z a-z]+$/) &&
      this.coursenameexist() == true){
      this.coursenameinvalid = false;
      this.success = false;
      this.coursenamevalid = true;
      return true;
    }

    else {
      this.coursenameinvalid = true;
      this.coursenamevalid = false;
      this.success = true;
      return false;
    }
  }


  classnameexists() : boolean {
    console.log('yeahan agae!');
    this._httpService.postCheckClassName(this.addcourse).subscribe(
      data => console.log('data value: ' + data.coursenomatch),
      error => {this.classnameexist = true,
        this.classnameinvalid=true,
        this.classnamevalid = false,
        this.success = true;
      },
      () => {
        this.classnameexist = false,
        this.classnamevalid=true ,
          this.classnameinvalid = false
         this.success = false;
      }
    );

    if(this.classnameexist == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }
  }


  coursenameexist() :boolean {
    console.log('yeahan agae!');
    this._httpService.postCheckCourseName(this.addcourse).subscribe(
      data => console.log('data value: ' + data.coursenamematch),
      error => {
        this.courseNameexist = true,
          this.coursenameinvalid = true,
          this.coursenamevalid = false
            this.success = true;
      },
      () => {
        this.courseNameexist = false,
          this.coursenamevalid=true,
          this.coursenameinvalid = false,
          this.success =false;
      }
    );

    if(this.courseNameexist == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }

  }


  Success(){

    $(".call_modal").show(function(){
      $(".modal").fadeIn();

    });

    $(document).ready(function(){
      $(".closer").click(function(){
        $(".modal").fadeOut();
      });
    });


  }


  addCourse() {

    if (this.addcourse.coursename != null && this.addcourse.coursename.match(/^[A-Z a-z]+$/)
      && this.addcourse.classname != null && this.addcourse.classname.match(/^[0-9]+$/) &&
      this.validateclassname() == true ) {

      console.log('here!');
      this._httpService.postAddCourse(this.addcourse).subscribe(
        data =>  console.log('data value: ' +data.coursenomatch),
        error => {
        },
        () => {

          this.classnamevalid= true,
            this.coursenamevalid = true,
            this.coursenameinvalid = false,
            this.success = false,
            console.log('added!')
        }
      );

      this.Success();
    }

    else {

      this.classnameinvalid= true,
        this.coursenameinvalid = true,
        this.generalmatch = true;
        this.success = true;
      console.log('not added!');
    }
  }


  clear(){
    this.addcourse.classname = null;
    this.addcourse.coursename = null;
    this.classnamevalid = false;
    this.classnameinvalid = false;
    this.coursenamevalid = false;
    this.coursenameinvalid = false;
    this.success = false;
  }


}

