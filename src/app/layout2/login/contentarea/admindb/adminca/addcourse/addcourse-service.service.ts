import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class AddcourseServiceService {

  constructor(private _http:Http) { }

  postCheckClassName(addcourse: any){

    console.log('okkk');
    console.log("Course no" +addcourse.classname);
    var body = {classname:addcourse.classname};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/returnclassname', body,
      {
        headers: headers,
      }).map(res => res.json())
  }


  postCheckCourseName(addcourse: any){
    console.log('okkk');
    console.log("Course Name" +addcourse.coursename );
    var body = {coursename:addcourse.coursename};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/coursenameexist', body,
      {
        headers: headers,
      }).map(res => res.json())
  }


  postAddCourse(addcourse){

    console.log('okkk');
    console.log("Course name"+addcourse.coursename + "Class name: "+addcourse.classname);
    var body={coursename:addcourse.coursename, classname:addcourse.classname};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/courses', body,
      {
        headers: headers
      }).map(res =>res.json())
  }


}
