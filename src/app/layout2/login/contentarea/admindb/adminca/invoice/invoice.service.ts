import { Injectable } from '@angular/core';

import {Http, Response, Headers} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class InvoiceService {

  constructor(private _http: Http) { }


  getRecord(v: any){
    let url = 'http://localhost:3000/admins/getrecordofstudent/'+v;
    return this._http.get(url).map((response:Response) => response.json());

  }

}
