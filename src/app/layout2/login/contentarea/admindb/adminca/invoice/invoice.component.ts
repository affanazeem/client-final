import { Component, OnInit } from '@angular/core';
import {InvoiceService} from "./invoice.service";
import {FeesserviceService} from "../managefees/feesservice.service";

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  id: any;
  accountno: any;
  array:any = [];
  p: any;
  ap: any;
  tot: any;
  grand: any;
  sub: any;
  constructor(private _httpService: InvoiceService, private _invoice: FeesserviceService) {

    this.id = this._invoice.id;

  }

  ngOnInit() {

    this._httpService.getRecord(this.id).subscribe(
      data => {
        this.array = data;

        this.p = (parseInt("" + this.array.annualmisc, 0) + parseInt("" + this.array.milladmisc, 0) + parseInt("" + this.array.healthcaremisc, 0));

        this.ap = parseInt(""+this.array.amountpaid,0)- this.p;

        this.tot = parseInt("" + this.array.annualmisc, 0) + parseInt("" + this.array.milladmisc, 0) + parseInt("" + this.array.healthcaremisc, 0) + this.ap;

        this.sub = this.ap - this.array.fine;

        this.grand = this.tot+ parseInt("" + this.array.fine, 0);

      });

  }

}
