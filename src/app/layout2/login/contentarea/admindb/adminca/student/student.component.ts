import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import * as $ from 'jquery';
import {StudentserviceService} from "./studentservice.service";
@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {


  constructor(private router: Router, private _httpService: StudentserviceService) {
  }

  studentnameexist: boolean;

  usernameinvalid: boolean = true;
  usernamevalid: boolean = false;

  disable: boolean = false;
  nameinvalid: boolean = true;
  namevalid: boolean = false;
  passlen: boolean = false;

  lnameinvalid: boolean = true;
  lnamevalid: boolean = false;

  classidinvalid: boolean = true;
  claasidvalid: boolean = false;

  secinvalid: boolean = true;
  secvalid: boolean = false;

  phoneinvalid: boolean = true;
  phonevalid: boolean = false;

  emailinvalid: boolean = true;
  emailvalid: boolean = false;

  parentemailinvalid: boolean = true;
  parentemailvalid: boolean = false;

  passinvalid: boolean;
  passvalid: boolean;

  phone_valid: any = /^[0-9]+$/;

  valid_classid: any = /^[0-9]+$/;


  instructor: any = {
    username: '',
    firstname: '',
    lastname: '',
    gender: '',
    classid: '',
    sec: '',
    phone: '',
    email: '',
    parentemail: '',
    password: ''
  }
  instruct_array = [];
  id: number = 0;

  fname: any = /^[A-Z a-z]+$/;
  phone: any = /^[0-9]+$/;
  class: any = /^[0-9]+$/;

  ngOnInit() {

    $(".modal_invalid").hide();

    this.nameinvalid = true;
    this.namevalid = false;

    this.lnameinvalid = true;
    this.lnamevalid = false;

    this.classidinvalid = true;
    this.claasidvalid = false;

    this.secinvalid = true;
    this.secvalid = false;

    this.phoneinvalid = true;
    this.phonevalid = false;


    this.emailinvalid = true;
    this.emailvalid = false;

    this.parentemailinvalid = true;
    this.parentemailvalid = false;

    this.passinvalid;
    this.passvalid;

  }

  validatename(): boolean {
    if (this.instructor.firstname != null && this.instructor.firstname.match(this.fname)) {
      this.namevalid = true;
      this.nameinvalid = false;
      return true;
    }
    else {
      this.namevalid = false;
      this.nameinvalid = true;
      return true;
    }
  }

  validatelname(): boolean {
    if (this.instructor.lastname != null && this.instructor.lastname.match(this.fname)) {
      this.lnamevalid = true;
      this.lnameinvalid = false;
      return true;
    }
    else {
      this.lnamevalid = false;
      this.lnameinvalid = true;
      this.disable = true;
      return false;
    }
  }


  validateID(): boolean {
    if (this.instructor.classid != null && this.instructor.classid.match(this.class) && this.instructor.classid.length == (1 || 2)) {
      this.valid_classid = true;
      this.classidinvalid = false;
      return true;
    }
    else {
      this.valid_classid = false;
      this.classidinvalid = true;
      this.disable = true;
      return false;
    }
  }

  validateSec(): boolean {
    if (this.instructor.sec != null && (this.instructor.sec == "A" || this.instructor.sec == "B" || this.instructor.sec == "C" || this.instructor.sec == "D")) {
      this.secvalid = true;
      this.secinvalid = false;
      return true;
    }
    else {
      this.secvalid = false;
      this.secinvalid = true;
      this.disable = true;
      return false;
    }
  }


  validatephone(): boolean {
    if (this.instructor.phone != null && this.instructor.phone.match(this.phone) && this.instructor.phone.length == 11) {
      this.phonevalid = true;
      this.phoneinvalid = false;
      return true;
    }
    else {
      this.phonevalid = false;
      this.phoneinvalid = true;
      this.disable = true;
      return false;
    }
  }


  validateemail(): boolean {
    if (this.instructor.email != null && this.instructor.email.match(/^[A-z A-Z]*[0-9]*@[a-z A-Z]*[.][a-z A-Z]+$/)) {
      this.emailvalid = true;
      this.emailinvalid = false;
      return true;
    }
    else {
      this.emailvalid = false;
      this.emailinvalid = true;
      this.disable = true;
      return false;
    }
  }

  validateparentemail(): boolean {
    if (this.instructor.parentemail != null && this.instructor.parentemail.match(/^[A-z A-Z]*[0-9]*@[a-z A-Z]*[.][a-z A-Z]+$/)) {
      this.parentemailvalid = true;
      this.parentemailinvalid = false;
      $('.modal_same').show();
      return true;
    }

    else {
      this.parentemailvalid = false;
      this.parentemailinvalid = true;
      this.disable = true;
      return false;
    }
  }

  validatepassword(): boolean {

    if (this.instructor.password != null && this.instructor.password.length >= 8) {
      this.passvalid = true;
      this.passinvalid = false;
      this.passlen = false;
      return true;
    }
    else {
      this.passvalid = false;
      this.passinvalid = true;
      this.passlen = true;
      this.disable = true;
      return false;
    }
  }

  validatelength(): boolean {
    if (this.instructor.password.length < 8) {
      this.passinvalid = true;
      this.passlen = true;
      return false;
    }
    else {
      this.passvalid = true;
      this.passlen = false;
      this.disable = true;
      return true;
    }
  }


  validateusername(): boolean {

    console.log('agaya');
    if (this.instructor.username != null && this.instructor.username.match(/^[A-Z a-z]+$/) &&
      this.Studentnameexist() == true) {
      console.log('ok');
      this.usernameinvalid = false;
      this.usernamevalid = true;
      return true;

    }
    else {
      this.usernameinvalid = true;
      this.usernamevalid = false;
      return false;
    }
  }

  Studentnameexist(): boolean {
    this._httpService.postCheckusername(this.instructor).subscribe(
      data => console.log('data value:' + data.username),
      error => {
        this.studentnameexist == true, this.usernameinvalid = true, this.usernamevalid = false
      },
      () => {
        this.studentnameexist = false, this.usernamevalid = true, this.usernameinvalid = false
      }
    );

    if (this.studentnameexist == true) {
      return false;
    }
    else {
      return true;
    }

  }


  create() {
    if (this.validateemail() == true && this.validateID() == true && this.validatelength() == true && this.validatelname() == true
      && this.validatename() == true && this.validateparentemail() == true && this.validatepassword() == true && this.validateSec() == true
      && this.validatephone() == true && this.validateusername() == true) {
      console.log('okk done wit the std!');

      this._httpService.postAddstudent(this.instructor).subscribe(
        data => {
        },
        error => {
          // $(".modal_invalid").show();
        },
        () => {
          $(".modal_invalid").hide();
          this.clear(),
            $(".call_modal").show(function () {
              $(".modal_main").fadeIn();

            });

          $(document).ready(function () {
            $(".closer").click(function () {
              $(".modal_main").fadeOut();
            });
          });
        },);
    }

    else {
      this.passlen = false;
      this.passinvalid = true;
      this.passvalid = false;
      $(".modal_invalid").show();
    }
  }

  cancel() {
    $(".modal_invalid").hide();
  }

  clear() {

    this.instructor.username = '',
      this.instructor.username = '',
      this.instructor.firstname = '',
      this.instructor.lastname = '',
      this.instructor.gender = '',
      this.instructor.phone = '',
      this.instructor.email = '',
      this.instructor.password = '',

      this.usernameinvalid = false;
    this.usernamevalid = false;

    this.nameinvalid = false;
    this.namevalid = false;

    this.lnameinvalid = false;
    this.lnamevalid = false;

    this.phoneinvalid = false;
    this.phonevalid = false;


    this.emailinvalid = false;
    this.emailvalid = false;

    this.passinvalid = false;
    this.passvalid = false;

    this.secinvalid = false;
    this.secvalid = false;

    this.classidinvalid = false;
    this.valid_classid = false;


  }

}
