import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class StudentserviceService {
  constructor(private _http:Http) { }



  postCheckClassName(instructor: any){

    console.log('okkk');
    console.log("Class name" +instructor.classid);
    var body = {classname:instructor.classid};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/findclassname', body,
      {
        headers: headers,
      }).map(res => res.json())
  }


  postCheckusername(studentusername :any)
  {
    console.log("Student name"+ studentusername.username);
    var body = {username:studentusername.username};
    var headers =new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/Validstudentname' ,body,
      {
        headers:headers,
      }

    ).map(res => res.json())
  }




  postAddstudent(instructor :any)
  {
    console.log('done!');
    var body = {username:instructor.username, firstname:instructor.firstname,
      lastname:instructor.lastname,gender:instructor.gender,classId:instructor.classid,sectionname:instructor.sec,phone:instructor.phone,
      email:instructor.email,parentemail:instructor.parentemail, password:instructor.password,expire:false};
    var headers =new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/addStudent' ,body,
      {
        headers:headers,
      }

    ).map(res => res.json())
  }


}

