import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import {FeesserviceService} from "./feesservice.service";

@Component({
  selector: 'app-managefees',
  templateUrl: './managefees.component.html',
  styleUrls: ['./managefees.component.css']
})
export class ManagefeesComponent implements OnInit {

  constructor(private router: Router, private _httpService: FeesserviceService) {
  }

  ae: any;
  mf: any;
  add: boolean = false;
  delete: boolean = false;
  view: boolean = false;
  edit: boolean = false;
  notfined: boolean;
  fined: boolean;
  collect: any;
  c: boolean = false;
  pay: any;
  p: boolean = false;
  alreadyexist:boolean;
  notalreadyexist:boolean;

  ac: number = 0;


  accountexist: boolean= false;


  accountnovalid:boolean;
  accountnoinvalid:boolean;

  namevalid:boolean= false;
  nameinvalid:boolean= false;

  firstnamevalid:boolean;
  firstnameinvalid:boolean;

  lastnamevalid:boolean = false;
  lastnameinvalid:boolean = false;

  amountpaidvalid:boolean = false;
  amountpaidinvalid:boolean = false;

  annualmiscvalid: boolean = false;
  annualmiscinvalid: boolean = false;

  milladmiscvalid: boolean = false;
  milladmiscinvalid: boolean = false;

  amountrecievedvalid:boolean = false;
  amountrecievedinvalid:boolean = false;


  duedateinvalid:boolean= false;
  duedatevalid: boolean= false;

  dateAmountRecievedinvalid: boolean= false;
  dateAmountRecievedvalid: boolean= false;

  miscinvalid: boolean= false;
  miscvalid: boolean= false;

  nameexist : boolean;

  feesnameexist:boolean;

  fname : any = /^[A-Z a-z]+$/;
  lname : any = /^[A-Z a-z]+$/;
  ddate = /^\d{2}([./-])\d{2}\1\d{4}$/;
  amountrec : any = /^[0-9]+$/;
  accountNO :any=/^[0-9]+$/;
  amount:any=/^[0-9]+$/;

  addrecord: any = {
    accountno: '',
    firstname: '',
    lastname: '',
    amountpaid: '',
    duedate: '',
    annualmisc: '',
    milladmisc: '',
    healthcaremisc:'',
    dateamountrecieved: '',
    amountrecieved: '',
    onTime:'',
    fine:'',
  };

 delid: any;

  array: any = [];
  array_rec: any = [];
  ngOnInit() {


    this.notfined=false;
    this.fined =false;

    this.notalreadyexist=false;
    this.alreadyexist=false;

    this.accountnoinvalid=false;
    this.accountnovalid=false;

    this.nameinvalid=false;
    this.namevalid=false;

    this.firstnameinvalid=false;
    this.firstnamevalid=false;

    this.lastnameinvalid=false;
    this.lastnamevalid=false;

    this.amountpaidinvalid=false;
    this.amountpaidvalid=false;

    this.duedateinvalid=false;
    this.duedatevalid=false;

    this.miscinvalid=false;
    this.miscvalid=false;

    this.dateAmountRecievedinvalid=false;
    this.dateAmountRecievedvalid=false;

    this.amountrecievedinvalid=false;
    this.amountrecievedvalid=false;

    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });

    $(".fa-plus").click(function () {
      console.log('at add');
      $(".modal_faculty").fadeOut();
    });


    $(".fa-eye").click(function () {
      console.log('at view');
      $(".modal_faculty").fadeOut();
    });
  }

  validateAccountno(){
    if(this.addrecord.accountno!= null && this.addrecord.accountno.match(/^[0-9]+$/) && this.account()==true){
      this.accountnovalid = true;
      this.accountnoinvalid = false;
      return true;
    }
    else
    {
      this.accountnoinvalid = true;
      this.accountnovalid = false;
    }
  }

  account():boolean{
    this._httpService.getAccount(this.addrecord).subscribe(
      data => console.log("data value"+data.accountno),
      error => {this.accountexist = true, this.accountnoinvalid =true, this.accountnovalid =false},
      ()=>{  this.accountexist = false ,this.accountnoinvalid =false, this.accountnovalid =true}
    );
    if(this.accountexist == true){
      return false;
    }
    else
    {

      console.log('error!');
      return true;
    }
  }

  validatename(){
    if (this.addrecord.firstname != null && this.addrecord.firstname.match(this.fname)) {
      this.firstnamevalid = true;
      this.firstnameinvalid = false;
    }
    else {
      this.firstnamevalid = false;
      this.firstnameinvalid = true;
    }
  }

  validatelname() {

    if (this.addrecord.lastname != null && this.addrecord.lastname.match(this.lname) && this.feesname()==true && this.name()==true) {
      this.lastnamevalid = true;
      this.lastnameinvalid = false;
    }
    else {
      this.lastnamevalid = false;
      this.lastnameinvalid = true;
    }
  }

  name():boolean{

    this._httpService.getStudent(this.addrecord).subscribe(
      data => console.log(data),
      error => {
        this.nameexist == true;
        this.nameinvalid=true;
        this.namevalid =false;
      },
      ()=> {this.nameexist == false , this.nameinvalid=false, this.namevalid=true });

    if(this.nameexist == true){
      return false;
    }
    else
    {
      return true;
    }
  }


  feesname():boolean
  {
    this._httpService.getStudentName(this.addrecord).subscribe(
      data => console.log(data),
      error => {
        this.feesnameexist == true , this.nameinvalid=true , this.namevalid =false, this.alreadyexist=true, this.notalreadyexist=false},
      ()=> {this.feesnameexist == false , this.nameinvalid=false, this.namevalid=true ,this.alreadyexist=false,this.notalreadyexist=true });

    if(this.feesnameexist == true){
      return false;
    }
    else
    {
      return true;
    }

  }

  validatedate() {
    if (this.addrecord.duedate != null && this.addrecord.duedate.match(this.ddate)) {
      this.duedatevalid = true;
      this.duedateinvalid = false;
    }
    else {
      this.duedatevalid = false;
      this.duedateinvalid = true;
    }
  }



  addRecord(){
    this.add = true;
    this.notfined = false;
    this.fined = false;

    $(".call_modal_faculty").hide(function () {
      // $(".modal_faculty").fadeIn();
    });
    console.log('i am in add record!');
  }

  viewRecord() {this.view = true;
    this.add = false;

    console.log('yeahn agaye!');
    this._httpService.getrecord().subscribe(
      data =>{
        this.array = data,
        console.log(this.array.length)}
    );



    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });

    console.log('in view record!');
  }




  validateDate() {
    var dd = this.addrecord.duedate;
    var rec_date = this.addrecord.dateamountrecieved;
    console.log(dd);
    console.log(rec_date);

    if(dd == ""){
      console.log('enter date!');
    }
    else {
      if (dd < rec_date) {
        this.fined = true;
        this.notfined = false;
        this.addrecord.onTime = "false";
        this.addrecord.fine = 500;
        console.log(this.addrecord.onTime);
      }
      else {
        this.notfined = true;
        this.fined = false;
        this.addrecord.onTime = "true";
        this.addrecord.fine = 0;
        console.log(this.addrecord.onTime);
      }
    }
  }


  addmisc() {

    if (this.addrecord.amountpaid == '') {
      alert("please enter the amount to be paid!");
    }
    else {
      console.log("at misc");

      $(".call_modal_misc").show(function () {
        $(".modal_misc").fadeIn();
      });
      $(".closer_misc").click(function () {
        $(".modal_misc").fadeOut();
      });
    }
  }

  addMiscCharges(amountpaid) {

    if(this.addrecord.annualmisc == null && this.addrecord.milladmisc == null && this.addrecord.healthcaremisc== null){
      console.log('chal gaya');
      amountpaid = parseInt(amountpaid);
      this.addrecord.amountpaid = amountpaid;
      $(".modal_misc").fadeOut();
      $(".modal_misc").hide();

    }
    else {
      console.log('yeah nhi chala');
      amountpaid = parseInt(amountpaid);
      amountpaid = parseInt("" + this.addrecord.annualmisc, 0) + parseInt("" + this.addrecord.milladmisc, 0) + parseInt("" + this.addrecord.healthcaremisc, 0) + amountpaid;

      if(this.addrecord.fine != 0){
      amountpaid = amountpaid+this.addrecord.fine;
      console.log("with fine"+amountpaid);
      }

      this.addrecord.amountpaid = amountpaid;
      console.log(amountpaid);
      $(".modal_misc").fadeOut();
      $(".modal_misc").hide();

    }
  }

  compare(){
    console.log("in compare");
    if(this.addrecord.amountrecieved < this.addrecord.amountpaid){
      this.c = true;
      this.collect = parseInt(""+this.addrecord.amountpaid) - parseInt(""+this.addrecord.amountrecieved);
      console.log("collect: "+this.collect);
    }

    else if(this.addrecord.amountrecieved > this.addrecord.amountpaid){
      this.c = false;
      this.p = true;
      this.pay = parseInt(""+this.addrecord.amountrecieved) - parseInt(""+this.addrecord.amountpaid);
      console.log("pay: "+this.pay);
    }
  }

  added(){
    this.ac++;
    this.addrecord.accountno = this.ac;
    console.log(this.addrecord.accountno);
  }

  create() {

    if (this.addrecord.accountno != null && this.addrecord.accountno.match(this.accountNO) && this.addrecord.firstname != null && this.addrecord.firstname.match(this.fname)
      && this.addrecord.lastname != null && this.addrecord.lastname.match(this.lname) &&
      this.addrecord.amountpaid != null && this.addrecord.duedate != null && this.addrecord.dateamountrecieved != null
      && this.addrecord.amountrecieved != null && this.addrecord.amountrecieved.match(/^[0-9]+$/)
    )
    {
      console.log('kaam sae hai!');

      console.log("on time"+this.addrecord.onTime);
      this._httpService.getallfees(this.addrecord).subscribe(
        data => {console.log(data)

        this.addrecord = '';
        },
        error => {},
        () => {alert('Student Record Entered Successfully!');})

    }
    else
    {
      alert('Invalid Entry');
    }
  }





  deleted(addrecord: any) {

    $(".modal_del").show();
    for (var i = 0; i < this.array.length; i++) {
      if (addrecord.id == this.array[i].id) {
          this.delid = addrecord.id;
          console.log('universal id'+this.delid);

      }
    }
  }


  ye(addrecord: any){
    $(".modal_del").hide();
    console.log('id'+addrecord.id);
    console.log("ARRAY LENGTH: "+this.array.length);

    for (var i = 0; i < this.array.length; i++) {
      if (this.delid == this.array[i].id) {
        this.array.splice(i, 1);
        this._httpService.DeleteRecord(this.delid).subscribe(
         data => {},
        error => {console.log('error')},

          ()=>{},
        );
      }
    }
  }

  no(){
      $(".modal_del").hide();
  }



  edited(v:any){
    $(".call_modal_edit").show(function () {
      $(".modal_edit").fadeIn();
    });
    $(".closer_edit").click(function () {
      $(".modal_edit").fadeOut();
    });

    console.log('ts mai aya');

    for(var i = 0 ; i < this.array.length; i++){
    if(v.id == this.array[i].id){

        this._httpService.getrecordfor(v.id).subscribe(
        data => {

          console.log('ok:'+v.id);
          this.array_rec = data;
          this.addrecord.accountno = this.array_rec.accountno;
          this.addrecord.firstname = this.array_rec.firstname;
          this.addrecord.lastname = this.array_rec.lastname;
          this.addrecord.duedate = this.array_rec.duedate;
          this.addrecord.dateamountrecieved = this.array_rec.dateamountrecieved;
          this.addrecord.annualmisc = this.array_rec.annualmisc;
          this.addrecord.milladmisc = this.array_rec.milladmisc;
          this.addrecord.healthcaremisc = this.array_rec.healthcaremisc;
          this.addrecord.amountpaid = this.array_rec.amountpaid;
          this.addrecord.amountrecieved = this.array_rec.amountrecieved;
          this.validateDate();
          console.log(this.array_rec.accountno);
        });
    //
    }

    }

  }


  updateRecord(v: any){

      $(".modal_edit").hide();

      $(".modal_delete").show();
  }

  y(v: any){
    $(".modal_delete").hide();

    this._httpService.updateApprove(v).subscribe(
      data => {},
      error => {},
    )

  }

  n(){
    // $(".modal_faculty").hide();
    $(".modal_delete").hide();


  }


  invoice(v: any){
  for(var i=0 ; i < this.array.length; i++){

    if(v.id == this.array[i].id){

     this._httpService.getinvoice(v.id);
    this.router.navigate(['/admindb/adminca/invoice']);

    }
  }
 }



}

