import { Injectable } from '@angular/core';
import {Http, Response, Headers} from "@angular/http";
import 'rxjs/add/operator/map';


@Injectable()
export class FeesserviceService {

  constructor(private _http:Http) { }

  id: any;

  getinvoice(id: any){
  this.id = id;
  console.log("this id "+this.id);
  }

  getAccount(name:any)
  {
    console.log("account no"+ name.accountno);

    let url = 'http://localhost:3000/admins/getAccount/'+name.accountno;
    return this._http.get(url).map((response:Response) => response.json());

  }

  getStudent(name:any){
    console.log("name"+name.firstname+name.lastname)
    let url = 'http://localhost:3000/admins/getStudentByName/'+name.firstname+'/'+name.lastname;
    return this._http.get(url).map((response:Response) => response.json());

  }

  getStudentName(name:any){

    var body = {firstname:name.firstname, lastname:name.lastname};
    var headers =new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/StudentNameInFees' ,body,
      {
        headers:headers,
      }

    ).map(res => res.json())
  }

  DeleteRecord(addrecord:number){
    let url = 'http://localhost:3000/admins/DeleteFees/'+addrecord;
    return this._http.delete(url).map((response:Response) => response.json());
  }

  getrecord(){
    console.log('no 2')
    return this._http.get('http://localhost:3000/admins/getrecordoffees').map((response:Response) => response.json());
  }

  getrecordfor(addrecord:any){
    console.log('service mai aya hai');
    let url ='http://localhost:3000/admins/getrecordforfees/'+addrecord;
    return this._http.get(url).map((response:Response) => response.json());
  }

  getallfees(addrecord :any)
  {

      console.log("in service: "+addrecord.onTime);

      var body = {accountno:addrecord.accountno, firstname:addrecord.firstname,
      lastname:addrecord.lastname, amountpaid:addrecord.amountpaid, duedate:addrecord.duedate,
      annualmisc:addrecord.annualmisc,milladmisc:addrecord.milladmisc,healthcaremisc:addrecord.healthcaremisc,
      dateamountrecieved:addrecord.dateamountrecieved,amountrecieved:addrecord.amountrecieved,onTime:addrecord.onTime,fine:addrecord.fine};
    var headers =new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/addfees' ,body,
      {
        headers:headers,
      }

    ).map(res => res.json())
  }

  updateApprove(v: any){

    var body={accountno:v.accountno,firstname:v.firstname,lastname:v.lastname,amountpaid:v.amountpaid,duedate:v.duedate,annualmisc:v.annualmisc,
      milladmisc:v.milladmisc,healthcaremisc:v.healthcaremisc,dateamountrecieved:v.dateamountrecieved,amountrecieved:v.amountrecieved
      ,onTime: v.onTime, fine:v.fine
    };
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/UpdateFees/'+v.accountno+'/'+v.firstname+'/'+v.lastname+'/'+v.amountpaid+'/'+v.duedate+'/'+v.annualmisc+'/'+v.milladmisc
      +'/'+v.healthcaremisc+'/'+v.dateamountrecieved+'/'+v.amountrecieved+'/'+v.onTime+'/'+v.fine;
    return this._http.put(url,body,headers).map((response:Response) => response.json());
  }
}
