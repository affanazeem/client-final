import { TestBed, inject } from '@angular/core/testing';

import { FeesserviceService } from './feesservice.service';

describe('FeesserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FeesserviceService]
    });
  });

  it('should ...', inject([FeesserviceService], (service: FeesserviceService) => {
    expect(service).toBeTruthy();
  }));
});
