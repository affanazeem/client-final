import { Component, OnInit } from '@angular/core';
import {DefaulterService} from "./defaulter.service";

@Component({
  selector: 'app-defaulter',
  templateUrl: './defaulter.component.html',
  styleUrls: ['./defaulter.component.css']
})
export class DefaulterComponent implements OnInit {


  constructor(private _httpService: DefaulterService) { }

  array :any = [];


  ngOnInit() {
    this._httpService.getAllDefaulters().subscribe(

      data => {

        this.array = data;
        console.log(this.array);
      },
      error => {},
      () => {}

    )
  }

}
