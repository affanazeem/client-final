import { TestBed, inject } from '@angular/core/testing';

import { DefaulterService } from './defaulter.service';

describe('DefaulterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DefaulterService]
    });
  });

  it('should ...', inject([DefaulterService], (service: DefaulterService) => {
    expect(service).toBeTruthy();
  }));
});
