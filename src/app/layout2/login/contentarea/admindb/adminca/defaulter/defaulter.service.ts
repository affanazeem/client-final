import { Injectable } from '@angular/core';

import {Headers, Http, Response} from "@angular/http";

@Injectable()
export class DefaulterService {


  constructor(private _http:Http) { }

  classname:any;
  sectionname:any;
  subjectname:any;

  getAllDefaulters(){
    let url = 'http://localhost:3000/admins/Defaulters/';
    return this._http.get(url).map((response:Response) => response.json());
  }

}
