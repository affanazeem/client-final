import { Injectable } from '@angular/core';
import {Http, Response, Headers} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class ManagecourseserviceService {

  constructor(private _http: Http) {
  }

  getAllCourses() {
    console.log('okkk');

  return this._http.get('http://localhost:3000/admins/viewAllCourses').map((response:Response) => response.json());
}

  DeleteRecord(v:number){
    let url = 'http://localhost:3000/admins/DeleteCourseByCourseNo/'+v;
    return this._http.delete(url).map((response:Response) => response.json());
}

  getRecord(v:any){
    let url = 'http://localhost:3000/admins/getrecordofcourse/'+v;
    return this._http.get(url).map((response:Response) => response.json());
  }

  updateRecord(v:any){

    console.log("V"+v.id);
    console.log("Course No:"+v.courseno + "Course name: "+ v.coursename + "Credit Hours: "+ v.credithours);
    var body={courseno:v.courseno,coursename:v.coursename,credithours:v.credithours};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/UpdateCourse/'+v.id;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }

}


