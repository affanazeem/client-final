import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

import {ManagecourseserviceService} from "./managecourseservice.service";

@Component({
  selector: 'app-managecourse',
  templateUrl: './managecourse.component.html',
  styleUrls: ['./managecourse.component.css']
})
export class ManagecourseComponent implements OnInit {

coursenameinvalid: boolean;
coursenamevalid: boolean;
coursenoinvalid: boolean;
coursenovalid: boolean;
credithoursvalid: boolean;
credithoursinvalid : boolean;



array: any = [];
v: any = {courseno:'', coursename:'', credithours:''};
temp: any;

constructor(private _httpService: ManagecourseserviceService) {
}

ngOnInit() {
  console.log('here!');
  this._httpService.getAllCourses().subscribe(
    data => this.array = data);
}

edit(v)
{

  console.log('clicked!');
  $(".call_modal_faculty").show(function () {
    $(".modal_faculty").fadeIn();
  });
  $(".closer_faculty").click(function () {
    $(".modal_faculty").fadeOut();
  });
  this.array = '';
  this._httpService.getRecord(v.id).subscribe(
    data => {
      this.array = data
      console.log(this.array.length)
    });
}

updateCourse(v){
  this._httpService.updateRecord(v).subscribe(
    data => this.array = data,
    error => console.log('error!'),
  )

}




delete(v){
  for (var i = 0; i < this.array.length; i++) {
    if (v.id == this.array[i].id) {
      this.array.splice(i, 1);
      this._httpService.DeleteRecord(v.id).subscribe(
        data => this.array = data);
      error => console.log('error')
      }
    }
  }
}
