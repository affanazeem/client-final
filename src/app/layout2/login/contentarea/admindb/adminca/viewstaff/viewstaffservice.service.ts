import { Injectable } from '@angular/core';
import {Response, Http, Headers} from '@angular/http';

@Injectable()
export class ViewstaffserviceService {

  constructor(private _http:Http) { }

  getAllstaff(){
    console.log('okkk');

    return this._http.get('http://localhost:3000/admins/viewAllStaff').map((response:Response) => response.json());
}
  DeleteRecord(v:number){


    let url = 'http://localhost:3000/admins/DeleteStaff/'+v;
    return this._http.delete(url).map((response:Response) => response.json());
  }

  getRecord(v:any){
    console.log(v);
    let url = 'http://localhost:3000/admins/getrecordofstaff/'+v;
    return this._http.get(url).map((response:Response) => response.json());
  }

  updateRecord(v:any){

    console.log("V"+v.id);
    var body={username:v.username,firstname:v.firstname,lastname:v.lastname,
      gender: v.gender,interest:v.interest,phone:v.phone,email:v.email,password:v.password,isAdmin:v.isAdmin};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/UpdateStaff/'+v.id;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }

}
