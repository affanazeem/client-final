import { TestBed, inject } from '@angular/core/testing';

import { ViewstaffserviceService } from './viewstaffservice.service';

describe('ViewstaffserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewstaffserviceService]
    });
  });

  it('should ...', inject([ViewstaffserviceService], (service: ViewstaffserviceService) => {
    expect(service).toBeTruthy();
  }));
});
