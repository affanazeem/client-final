import { Component, OnInit } from '@angular/core';
import {ViewstaffserviceService} from "./viewstaffservice.service";
import * as $ from 'jquery';
@Component({
  selector: 'app-viewstaff',
  templateUrl: './viewstaff.component.html',
  styleUrls: ['./viewstaff.component.css']
})
export class ViewstaffComponent implements OnInit {


  array: any = [];
  v: any = [{username:"", firstname:"", lastname:"", gender: "",interest:"",phone:"",email:"",password:"",isAdmin:""}];

  constructor(private _httpService: ViewstaffserviceService) { }

  ngOnInit() {
    console.log('here!');
    this._httpService.getAllstaff().subscribe(
      data => this.array = data);
  }

  edit(v)
  {
    console.log('clicked!');
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
    this.array = '';
    this._httpService.getRecord(v.id).subscribe(
      data => {
        this.array = data
        console.log(this.array.length)
      });
  }

  updateCourse(v){
    this._httpService.updateRecord(v).subscribe(
      data => this.array = data,
      error => console.log('error!'),
    )

  }


  delete(v){
    for (var i = 0; i < this.array.length; i++) {
      if (v.id == this.array[i].id) {
        this.array.splice(i, 1);
        this._httpService.DeleteRecord(v.id).subscribe(
          data => this.array = data);
        error => console.log('error')
      }
    }
  }
}

