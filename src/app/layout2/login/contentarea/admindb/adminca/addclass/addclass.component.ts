import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

import {AddclassserviceService} from "./addclassservice.service";

@Component({
  selector: 'app-addclass',
  templateUrl: './addclass.component.html',
  styleUrls: ['./addclass.component.css']
})
export class AddclassComponent implements OnInit {

  addclass :any = {classname:''};
  assignclass:any = {classname:'',sectionname:''};

  constructor(private _httpService: AddclassserviceService) { }

  ngOnInit() {
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
  }

  num:any = /^[0-9]+$/;

  assignclassnameexist: boolean;
  assign: boolean;

  classnamematch: boolean = false;

  classnameinvalid: boolean;
  classnamevalid: boolean;


  assignclassinvalid: boolean;
  assignclassvalid: boolean;

  sectionnameinvalid: boolean;
  sectionnamevalid: boolean;

  classnameexists: boolean;

  classNameexist : boolean;
  success : boolean;
  assignsuccess: boolean;

  classexist():boolean{
    console.log('yeahan agae!');
    this._httpService.postCheckClassName(this.addclass).subscribe(
      data => console.log('data value: ' +data.classnameinvalid),
      error => {
        this.success = true,
          this.classnamematch = true,
          this.classnameinvalid=true,
          this.classnamevalid = false
      },
      () => {
        this.success = false,
        this.classnamematch = false,
        this.classnamevalid=true ,
        this.classnameinvalid = false
      }
    );

    if(this.classnamematch == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }
  }


  assignclassexist():boolean{
    console.log('yeahan agae!');
    this._httpService.postCheckAssignClassName(this.assignclass).subscribe(
      data => console.log('data value: ' +data.classnameinvalid),
      error => {
          this.assignsuccess = true,
          this.assignclassnameexist = true,
          this.assignclassinvalid=true,
          this.assignclassvalid = false
      },
      () => {
        this.assignsuccess = false,
          this.assignclassnameexist = false,
          this.assignclassvalid=true,
          this.assignclassinvalid = false
      }

    );

    if(this.assignclassnameexist == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }
  }



  validateclassname():boolean {
  console.log("LENGTH: "+this.addclass.classname.length);

    if(this.addclass.classname != null && this.addclass.classname.length<=2){
      if (this.addclass.classname == "1" || this.addclass.classname == "2"
        || this.addclass.classname == "3" || this.addclass.classname == "4" || this.addclass.classname == "5"
        || this.addclass.classname == "6" || this.addclass.classname == "7" || this.addclass.classname == "8" || this.addclass.classname == "9" ||
        this.addclass.classname == "10") {
        if (this.classexist()) {
          this.classnameinvalid = false;
          this.classnamevalid = true;
          this.success = false;
          return true;
        }
        else {
          this.classnameinvalid = true;
          this.classnamevalid = false;
          this.success = true;
        }
      }
        else {
          this.classnameinvalid = true;
          this.classnamevalid = false;
          this.success = true;
        }
      }

    else {
      console.log('yehan else mai agaya');
      this.classnameinvalid = true;
      this.classnamevalid = false;
      this.success = true;
      return false;
    }
  }


  validatesectionname():boolean {

    if(this.assignclass.sectionname != null && this.assignclass.sectionname.match(/^[A-D]+$/) && this.assignclass.sectionname.length ==1){
      this.sectionnameinvalid = false;
      this.sectionnamevalid = true;
      this.assignsuccess = false;
      return true;
    }

    else {
      this.sectionnameinvalid = true;
      this.sectionnamevalid = false;
      this.assignsuccess = true;
      return false;
    }
  }


  validateassignclassname():boolean {
    console.log("in assign class validation after click"+this.assignclass.classname);

    if(this.assignclass.classname != null && this.assignclass.classname.length<=2) {
      if (this.assignclass.classname == "1" || this.assignclass.classname == "2"
        || this.assignclass.classname == "3" || this.assignclass.classname == "4" || this.assignclass.classname == "5"
        || this.assignclass.classname == "6" || this.assignclass.classname == "7" || this.assignclass.classname == "8" || this.assignclass.classname == "9" ||
        this.assignclass.classname == "10") {
        if (this.assignclassexist()) {
          console.log('sb chal gaya name ka');
          this.assignclassinvalid = false;
          this.assignclassvalid = true;
          this.assignsuccess = false;
          return true;
        }
        else {
          this.assignclassinvalid = true;
          this.assignclassvalid = false;
          this.assignsuccess = true;
          return false;
        }
      }
    }

    else {
      this.assignclassinvalid = true;
      this.assignclassvalid = false;
      this.assignsuccess = true;
      return false;
    }
  }


  addClass() {
    if (this.validateclassname() == true) {
      console.log('here!');
      this._httpService.postAddClass(this.addclass).subscribe(
        data => console.log('data value: ' + data.classnamematch),
        error => {
        },
        () => {

          this.classnamevalid = true,
          this.classnameinvalid = false,
          this.success = false;
          console.log('added!')
        }
      );
    }

    else {
      this.success = true;
      console.log('not assigned!');
    }
  }


    assignClass() {
      if (this.validateassignclassname() == true && this.validatesectionname() == true) {

        console.log('here!');
        this._httpService.postAddAssignClass(this.assignclass).subscribe(
          data =>  console.log('data value: ' +data.classnamematch),
          error => {
            this.assign = false;
          },
          () => {

            this.assignclassvalid = true,
            this.assignclassinvalid = false,
            this.assignsuccess = false;
            console.log('added!')
          }
        );
          this.assign = false;
      }


      else {
      this.assignsuccess = true;
        this.assign = true;
      console.log('not assigned!');
    }
  }

clear(){
    this.addclass.classname = null;
    this.addclass.sectionname = null;
    this.classnamevalid = false;
    this.classnameinvalid = false;
    this.sectionnamevalid = false;
    this.sectionnameinvalid = false;
    this.assignsuccess = false;
    this.success = false;
    this.assign = false;
}




}
