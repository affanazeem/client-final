import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";

@Injectable()
export class AddclassserviceService {

  constructor(private _http: Http) { }


  postCheckClassName(addclass: any){
    console.log('okkk');
    console.log("Class name"+addclass.classname);
    var body={classname:addclass.classname};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/findclassname', body,
      {
        headers: headers
      }).map(res =>res.json())
  }


  postCheckAssignClassName(assignclass: any){
    console.log('okkk');
    console.log("Class name"+assignclass.classname);
    var body={classname:assignclass.classname};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/returnclassname', body,
      {
        headers: headers
      }).map(res =>res.json())
  }



  postAddClass(addclass: any){
    console.log('class name '+addclass.classname);
    var body={classname:addclass.classname};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/addClass', body,
      {
        headers: headers
      }).map(res =>res.json())
  }


  postAddAssignClass(assignclass: any){
    console.log('okkk');
    console.log("Class name"+assignclass.classname + "Section name"+assignclass.sectionname);

    var body={classname:assignclass.classname, sectionname:assignclass.sectionname};

    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/assignClass', body,
      {
        headers: headers
      }).map(res =>res.json())
  }

}
