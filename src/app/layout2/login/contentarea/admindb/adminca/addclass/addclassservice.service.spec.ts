import { TestBed, inject } from '@angular/core/testing';

import { AddclassserviceService } from './addclassservice.service';

describe('AddclassserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddclassserviceService]
    });
  });

  it('should ...', inject([AddclassserviceService], (service: AddclassserviceService) => {
    expect(service).toBeTruthy();
  }));
});
