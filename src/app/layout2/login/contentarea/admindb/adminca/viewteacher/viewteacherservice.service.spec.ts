import { TestBed, inject } from '@angular/core/testing';

import { ViewteacherserviceService } from './viewteacherservice.service';

describe('ViewteacherserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewteacherserviceService]
    });
  });

  it('should ...', inject([ViewteacherserviceService], (service: ViewteacherserviceService) => {
    expect(service).toBeTruthy();
  }));
});
