import { Component, OnInit } from '@angular/core';
import {ViewteacherserviceService} from "./viewteacherservice.service";
import * as $ from 'jquery';
@Component({
  selector: 'app-viewteacher',
  templateUrl: './viewteacher.component.html',
  styleUrls: ['./viewteacher.component.css']
})
export class ViewteacherComponent implements OnInit {
  array: any = [];

  constructor(private _httpService: ViewteacherserviceService) { }

  ngOnInit() {
    console.log('here!');
    this._httpService.getAllTeacher().subscribe(
      data => this.array = data);
  }
  delete(v){
    for (var i = 0; i < this.array.length; i++) {
      if (v.id == this.array[i].id) {
        this.array.splice(i, 1);
        this._httpService.DeleteRecord(v.id).subscribe(
          data => this.array = data);
        error => console.log('error')
      }
    }
  }
}
