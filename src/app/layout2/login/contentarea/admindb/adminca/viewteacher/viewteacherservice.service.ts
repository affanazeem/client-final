import { Injectable } from '@angular/core';
import {Response, Http} from '@angular/http';


@Injectable()
export class ViewteacherserviceService {

  constructor(private _http:Http) { }

  getAllTeacher(){
    console.log('okkk');

    return this._http.get('http://localhost:3000/admins/viewAllTeacher').map((response:Response) => response.json());
  }

  DeleteRecord(v:number){


    let url = 'http://localhost:3000/admins/DeleteTeacher/'+v;
    return this._http.delete(url).map((response:Response) => response.json());
  }


}
