import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsheetComponent } from './viewsheet.component';

describe('ViewsheetComponent', () => {
  let component: ViewsheetComponent;
  let fixture: ComponentFixture<ViewsheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
