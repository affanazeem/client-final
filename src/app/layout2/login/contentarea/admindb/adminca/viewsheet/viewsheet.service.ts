import { Injectable } from '@angular/core';
import {Http, Response, Headers} from "@angular/http";

@Injectable()
export class ViewsheetService {

  constructor(private _http:Http) { }

  getAllSchedule(classname){
    console.log(classname);
    let url = 'http://localhost:3000/admins/classWiseDateSheet/'+classname;
    return this._http.get(url).map((response:Response) => response.json());
  }


  DeleteRecord(v:number){
    console.log('in delete service');
    let url = 'http://localhost:3000/admins/DeleteExam/'+v;
    return this._http.delete(url).map((response:Response) => response.json());
  }


  updateRecord(v:any){

    var body={dateexam:v.dateexam,time:v.time,tname:v.tname,classname:v.classname,subjectname:v.subjectname};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/UpdateExam/'+v.dateexam+'/'+v.time+'/'+v.tname+'/'+v.classname+'/'+v.subjectname;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }


}
