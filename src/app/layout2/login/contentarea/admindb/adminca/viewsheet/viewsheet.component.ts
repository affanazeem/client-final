import { Component, OnInit } from '@angular/core';;
import {ViewsheetService} from "./viewsheet.service";
import {ViewService} from "../datesheet/view.service";
import * as $ from 'jquery';

@Component({
  selector: 'app-viewsheet',
  templateUrl: './viewsheet.component.html',
  styleUrls: ['./viewsheet.component.css']
})
export class ViewsheetComponent implements OnInit {

  classname: any;
  isEdit: boolean;
  dateinvalid: boolean;
  classinvalid: boolean;
  array_teacher: any = [];
  tnameinvalid: boolean;

  constructor(private _dsService : ViewService, private _httpService: ViewsheetService) {


    this.classname = this._dsService.classentered
    console.log(this.classname);

  }

  array :any = [];

  editres:any = {dateexam:'',time:'',tname:'',classname:'',subjectname:''};
  ngOnInit() {

    $(".modal_invalid").hide();

    this._httpService.getAllSchedule(this.classname).subscribe(

      data => {

        this.array = data;
        console.log(this.array);
      },
      error => {

        $(".call_modal_invalid").show(function () {
          $(".modal_invalid").fadeIn();
        });
      },
      () => {}

    )
  }

  cancel() {

    $(".btn-warning").click(function () {
      $(".modal_invalid").fadeOut();
    });

  }


  edit(v)
  {
    console.log('clicked!');
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });

    this.editres.dateexam = v.dateexam;
    this.editres.time = v.time;
    this.editres.tname = v.tname;
    this.editres.classname = v.classname;
    this.editres.subjectname = v.subjectname;

  }



  no(){
    this.isEdit = false;
    $(".btn-red").click(function () {
      console.log('no!');
      $(".modal_delete").fadeOut();
    });
  }

  yes(){

    this._httpService.updateRecord(this.editres).subscribe(
      data => this.array = data,
      error => console.log('error!'),
    )

    $(".btn-blue").click(function () {
      $(".modal_delete").fadeOut();
    });
    this.isEdit = true;
  }


  update(v: any){
      $(".call_modal_faculty").hide();
      $(".modal_faculty").fadeOut();
      console.log("clicked delete!");

      $(".call_modal_delete").show(function () {
        $(".modal_delete").fadeIn();
      });
    }



  delete(v: any) {
    console.log('outside for');
    for (var i = 0; i < this.array.length; i++) {
      console.log('in for');
      if (v.id == this.array[i].id) {
        this.array.splice(i, 1);
        this._httpService.DeleteRecord(v.id).subscribe(
          data => {
            // this.array = data
          },
          error => {
            console.log('error')
          },
          () => {
            console.log('complete')
          }
        )
      }
    }
  }



}
