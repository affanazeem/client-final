import { TestBed, inject } from '@angular/core/testing';

import { ViewsheetService } from './viewsheet.service';

describe('ViewsheetService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewsheetService]
    });
  });

  it('should ...', inject([ViewsheetService], (service: ViewsheetService) => {
    expect(service).toBeTruthy();
  }));
});
