import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecifycoursesComponent } from './specifycourses.component';

describe('SpecifycoursesComponent', () => {
  let component: SpecifycoursesComponent;
  let fixture: ComponentFixture<SpecifycoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecifycoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecifycoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
