import { TestBed, inject } from '@angular/core/testing';

import { SpecifycoursesService } from './specifycourses.service';

describe('SpecifycoursesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpecifycoursesService]
    });
  });

  it('should ...', inject([SpecifycoursesService], (service: SpecifycoursesService) => {
    expect(service).toBeTruthy();
  }));
});
