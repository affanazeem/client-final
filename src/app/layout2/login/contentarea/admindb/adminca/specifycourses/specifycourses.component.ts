import { Component, OnInit } from '@angular/core';
import {SpecifycoursesService} from "./specifycourses.service";
import * as $ from 'jquery';

@Component({
  selector: 'app-specifycourses',
  templateUrl: './specifycourses.component.html',
  styleUrls: ['./specifycourses.component.css']
})
export class SpecifycoursesComponent implements OnInit {


  addteacher: any = {firstname:'',lastname:'',coursename:''};


  firstnameinvalid: boolean;
  firstnamevalid: boolean;

  lastnameinvalid: boolean;
  lastnamevalid: boolean;

  coursenameinvalid: boolean;
  coursenamevalid: boolean;

  courseNameexist : boolean;
  firstNameexist : boolean;
  lastNameexist : boolean;


  success : boolean;

  constructor(private _httpService: SpecifycoursesService) { }
  ngOnInit() {
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
  }



  validatefirstname():boolean {

    if(this.addteacher.firstname != null && this.addteacher.firstname.match(/^[A-Z a-z]+$/)&&
      this.firstnameexists() ){
      this.firstnameinvalid = false;
      this.firstnamevalid = true;
      this.success = false;
      return true;
    }

    else {
      this.firstnameinvalid = true;
      this.firstnamevalid = false;
      this.success = true;
      return false;
    }
  }

  validatelastname():boolean {

    if(this.addteacher.lastname != null && this.addteacher.lastname.match(/^[A-Z a-z]+$/)&&
      this.lastnameexists() ){
      this.lastnameinvalid = false;
      this.lastnamevalid = true;
      this.success = false;
      return true;
    }

    else {
      this.lastnameinvalid = true;
      this.lastnamevalid = false;
      this.success = true;
      return false;
    }
  }


  validatecoursename():boolean {

    if(this.addteacher.coursename != null && this.addteacher.coursename.match(/^[A-Z a-z]+$/) &&
      this.coursenameexist() == true){
      this.coursenameinvalid = false;
      this.success = false;
      this.coursenamevalid = true;
      return true;
    }

    else {
      this.coursenameinvalid = true;
      this.coursenamevalid = false;
      this.success = true;
      return false;
    }
  }


  firstnameexists(){

    console.log('yeahan agae!');
    this._httpService.postCheckFirstName(this.addteacher).subscribe(
      data => console.log('data value: ' + data.firstnamematch),
      error => {
        this.firstNameexist = true,
          this.firstnameinvalid = true,
          this.firstnamevalid = false
        this.success = true;
      },
      () => {
        this.firstNameexist = false,
          this.firstnamevalid=true,
          this.firstnameinvalid = false,
          this.success =false;
      }
    );

    if(this.firstNameexist == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }

  }

  lastnameexists(){

    console.log('yeahan agae!');
    this._httpService.postCheckLastName(this.addteacher).subscribe(
      data => console.log('data value: ' + data.lastnamematch),
      error => {
        this.lastNameexist = true,
          this.lastnameinvalid = true,
          this.lastnamevalid = false,
          this.firstnameinvalid = true,
          this.firstnamevalid = false
        this.success = true;
      },
      () => {
        this.lastNameexist = false,
          this.lastnamevalid=true,
          this.lastnameinvalid = false,
          this.firstnamevalid=true,
          this.firstnameinvalid = false,

          this.success =false;
      }
    );

    if(this.lastNameexist == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }

  }


  coursenameexist() :boolean {
    console.log('yeahan agae!');
    this._httpService.postCheckCourseName(this.addteacher).subscribe(
      data => console.log('data value: ' + data.coursenamematch),
      error => {
        this.courseNameexist = true,
        this.coursenameinvalid = true,
        this.coursenamevalid = false
        this.success = true;
      },
      () => {
        this.courseNameexist = false,
          this.coursenamevalid=true,
          this.coursenameinvalid = false,
          this.success =false;
      }
    );

    if(this.courseNameexist == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }

  }

  specifyCourse(){

    if (this.addteacher.firstname != null && this.addteacher.firstname.match(/^[A-Z a-z]+$/) &&
      this.addteacher.lastname != null && this.addteacher.lastname.match(/^[A-Z a-z]+$/)
      && this.addteacher.coursename != null && this.addteacher.coursename.match(/^[A-Z a-z]+$/)
     &&  this.validatefirstname() == true &&  this.validatelastname() == true &&  this.validatecoursename() == true) {

      console.log('here!');
      this._httpService.postAddCourse(this.addteacher).subscribe(
        data =>  console.log('data value: ' +data.coursenomatch),
        error => {
          this.success = true
        },
        () => {

          this.firstnamevalid= true,

            this.lastnamevalid= true,
            this.coursenamevalid= true,

            this.firstnamevalid = true,
            this.firstnameinvalid = false,

            this.lastnamevalid = true,
            this.lastnameinvalid = false,

            this.coursenamevalid = true,
            this.coursenameinvalid = false,


            this.success = false,


            console.log('added!')
        }
      );
}

    else {

       this.firstnameinvalid= true,
         this.lastnameinvalid= true,
         this.coursenameinvalid= true,
        this.coursenameinvalid = true,
         this.firstnameinvalid = true,
         this.lastnameinvalid = true,
      this.success = true;
      console.log('not added!');
    }
  }


  clear(){
    this.addteacher.firstname = null;
    this.addteacher.lastname = null;
    this.addteacher.coursename = null;

    this.firstnamevalid = false;
    this.firstnameinvalid = false;

    this.lastnamevalid = false;
    this.lastnameinvalid = false;

    this.coursenamevalid = false;
    this.coursenameinvalid = false;

    this.success = false;
  }


}
