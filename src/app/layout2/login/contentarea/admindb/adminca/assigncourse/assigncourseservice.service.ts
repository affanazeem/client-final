import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AssigncourseserviceService {

  constructor(private _http: Http) {
  }

  postCheckCourseNo(addcourse: any){

    console.log('okkk');
    console.log("Course no" +addcourse.courseno );
    var body = {courseno:addcourse.courseno};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/findcourseno', body,
      {
        headers: headers,
      }).map(res => res.json())
  }


  postCheckCourseName(addcourse: any){
    console.log('okkk');
    console.log("Course Name" +addcourse.coursename );
    var body = {coursename:addcourse.coursename};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/findcoursename', body,
      {
        headers: headers,
      }).map(res => res.json())
  }



  postAddCourses(addcourses: any) {


    console.log('okkk');
    console.log("Course no" + addcourses.courseno + "Course name: " + addcourses.coursename + "Credit hours: " + addcourses.credithours);
    var body = {courseno:addcourses.courseno,coursename:addcourses.coursename,credithours:addcourses.credithours};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/addCourses', body,
      {
        headers: headers,
      }).map(res => res.json())
  }

}
