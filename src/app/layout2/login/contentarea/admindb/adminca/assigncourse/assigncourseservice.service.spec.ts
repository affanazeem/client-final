import { TestBed, inject } from '@angular/core/testing';

import { AssigncourseserviceService } from './assigncourseservice.service';

describe('AssigncourseserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssigncourseserviceService]
    });
  });

  it('should ...', inject([AssigncourseserviceService], (service: AssigncourseserviceService) => {
    expect(service).toBeTruthy();
  }));
});
