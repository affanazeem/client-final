import { Component, OnInit } from '@angular/core';
import {AssigncourseserviceService} from "./assigncourseservice.service";
import * as $ from  'jquery';


@Component({
  selector: 'app-assigncourse',
  templateUrl: './assigncourse.component.html',
  styleUrls: ['./assigncourse.component.css']
})
export class AssigncourseComponent implements OnInit {

  addcourse: any = {courseno:'', coursename: '',credithours:''};
  coursenomatch: boolean = false;
  coursenamematch: boolean = false;
  generalmatch: boolean = false;

  coursenovalid: boolean;
  coursenoinvalid: boolean;

  coursenameinvalid: boolean;
  coursenamevalid: boolean;

  credithoursinvalid: boolean;
  credithoursvalid: boolean;

  coursenoexist : boolean;
  courseNameexist : boolean;
  success : boolean;

  constructor(private _httpService: AssigncourseserviceService) { }

  ngOnInit() {
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
  }


  validatecourseno():boolean {

    if(this.addcourse.courseno != null && this.addcourse.courseno.match(/^[0-9]+$/) && this.addcourse.courseno.length == 3 &&
    this.courseexist() ){
      this.coursenoinvalid = false;
      this.coursenovalid = true;
      return true;
    }

    else {
      this.coursenoinvalid = true;
      this.coursenovalid = false;
      return false;
    }
  }

  validatecoursename():boolean {

    if(this.addcourse.coursename != null && this.addcourse.coursename.match(/^[A-Z a-z]+$/) &&
      this.coursenameexist() == true){
      this.coursenameinvalid = false;
      this.coursenamevalid = true;
      return true;
    }

    else {
      this.coursenameinvalid = true;
      this.coursenamevalid = false;
      return false;
    }
  }


  courseexist() : boolean {
  console.log('yeahan agae!');
  this._httpService.postCheckCourseNo(this.addcourse).subscribe(
    data => console.log('data value: ' + data.coursenomatch),
    error => {this.coursenoexist = true, this.coursenoinvalid=true, this.coursenovalid = false},
    () => {this.coursenoexist = false,this.coursenovalid=true , this.coursenoinvalid = false}
  );

  if(this.coursenoexist == true){
  return false;

  }
  else{
    console.log('chal gaya!');
    return true;
  }
}


  coursenameexist() :boolean {
    console.log('yeahan agae!');
    this._httpService.postCheckCourseName(this.addcourse).subscribe(
      data => console.log('data value: ' + data.coursenamematch),
      error => {this.courseNameexist = true, this.coursenameinvalid = true, this.coursenamevalid = false},
      () => {this.courseNameexist = false,this.coursenamevalid=true, this.coursenameinvalid = false}
    );

    if(this.courseNameexist == true){
      return false;

    }
    else{
      console.log('chal gaya!');
      return true;
    }

  }

  validatecredithours(){
    if(this.addcourse.credithours != null && this.addcourse.credithours.match(/^[0-9]+$/)){
      this.credithoursinvalid = false;
      this.credithoursvalid = true;
    }

    else {
      this.credithoursinvalid = true;
      this.credithoursvalid = false;
    }
  }


  Success(){

    $(".call_modal").show(function(){
      $(".modal").fadeIn();

    });

    $(document).ready(function(){
      $(".closer").click(function(){
        $(".modal").fadeOut();
      });
    });


  }


  addCourses() {

    if (this.addcourse.coursename != null && this.addcourse.coursename.match(/^[A-Z a-z]+$/)
      && this.addcourse.courseno != null && this.addcourse.courseno.length == 3 && this.addcourse.credithours != null
      && this.addcourse.credithours.match(/^[0-9]+$/) && this.validatecourseno() == true && this.validatecoursename() == true ) {

     console.log('here!');
      this._httpService.postAddCourses(this.addcourse).subscribe(
        data =>  console.log('data value: ' +data.coursenomatch),
        error => {
            this.coursenoinvalid= true,

            this.coursenameinvalid = true,
             this.coursenamevalid = false,
            this.credithoursinvalid = true,
            this.success = true;
        },
          () => {

            this.coursenovalid= true,
            this.coursenamevalid = true,
              this.coursenameinvalid = false,
            this.credithoursvalid = true,
            this.success = false,
            console.log('added!')
        }
      );

      this.Success();
    }

    else {
      this.generalmatch = true;
      this.success = true
      console.log('not added!');
    }
  }


  clear(){
    this.addcourse.courseno = null;
    this.addcourse.coursename = null;
    this.addcourse.credithours = null;

    this.coursenamevalid = false;
    this.coursenameinvalid = false;

    this.coursenovalid = false;
    this.coursenovalid = false;

    this.credithoursvalid = false;
    this.credithoursvalid = false;

    this.success = false;
  }



}
