import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmincaComponent } from './adminca.component';

describe('AdmincaComponent', () => {
  let component: AdmincaComponent;
  let fixture: ComponentFixture<AdmincaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmincaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmincaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
