import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-adminca',
  templateUrl: './adminca.component.html',
  styleUrls: ['./adminca.component.css']
})
export class AdmincaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(".login-html").fadeOut();
    $(".login-html").hide();
    $(".main").fadeOut();
    $(".main").hide();
  }

}
