import { Injectable } from '@angular/core';
import {Response, Http, Headers} from '@angular/http';

@Injectable()
export class ViewstdserviceService {

  constructor(private  _http:Http) { }


  getAllStudent(){
    console.log('okkk');

    return this._http.get('http://localhost:3000/admins/viewAllStudent').map((response:Response) => response.json());
  }

  DeleteRecord(v:number){


    let url = 'http://localhost:3000/admins/DeleteStudent/'+v;
    return this._http.delete(url).map((response:Response) => response.json());
  }

  getRecord(v:any){
    let url = 'http://localhost:3000/admins/getrecordofstudent/'+v;
    return this._http.get(url).map((response:Response) => response.json());
  }

  updateRecord(v:any){

    console.log("V"+v.id);
    var body={username:v.username,firstname:v.firstname,lastname:v.lastname,
      gender: v.gender,classId:v.classId,sectionname:v.sectionname,phone:v.phone,email:v.email,parentemail:v.parentemail,
      password:v.password,isAdmin:v.isAdmin};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/UpdateStudent/'+v.id;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }

}
