import { TestBed, inject } from '@angular/core/testing';

import { ViewstdserviceService } from './viewstdservice.service';

describe('ViewstdserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewstdserviceService]
    });
  });

  it('should ...', inject([ViewstdserviceService], (service: ViewstdserviceService) => {
    expect(service).toBeTruthy();
  }));
});
