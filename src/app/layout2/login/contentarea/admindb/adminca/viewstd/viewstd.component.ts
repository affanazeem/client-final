import { Component, OnInit } from '@angular/core';
import {ViewstdserviceService} from "./viewstdservice.service";
import * as $ from 'jquery';
@Component({
  selector: 'app-viewstd',
  templateUrl: './viewstd.component.html',
  styleUrls: ['./viewstd.component.css']
})
export class ViewstdComponent implements OnInit {


  array: any = [];
  v: any = [{username:"", firstname:"", lastname:"", gender: "",classId:"",sectionname:'',phone:"",email:"",parentemail:"",password:""}];


  constructor(private _httpService:ViewstdserviceService) { }

  ngOnInit() {
    console.log('here!');
    this._httpService.getAllStudent().subscribe(
      data => this.array = data);
  }


  delete(v){
    for (var i = 0; i < this.array.length; i++) {
      if (v.id == this.array[i].id) {
        this.array.splice(i, 1);
        this._httpService.DeleteRecord(v.id).subscribe(
          data => this.array = data);
        error => console.log('error')
      }
    }
  }



  edit(v)
  {
    console.log('clicked!');
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
    this.array = '';
    this._httpService.getRecord(v.id).subscribe(
      data => {
        this.array = data
        console.log(this.array.length)
      });
  }

  updateCourse(v){
    this._httpService.updateRecord(v).subscribe(
      data => this.array = data,
      error => console.log('error!'),
    )

  }


}
