import { TestBed, inject } from '@angular/core/testing';

import { AdminpostserviceService } from './adminpostservice.service';

describe('AdminpostserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminpostserviceService]
    });
  });

  it('should ...', inject([AdminpostserviceService], (service: AdminpostserviceService) => {
    expect(service).toBeTruthy();
  }));
});
