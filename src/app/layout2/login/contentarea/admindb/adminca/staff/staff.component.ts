import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import * as $ from 'jquery';
import {StaffserviceService} from "./staffservice.service";

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {
  constructor(private router: Router,private _httpService: StaffserviceService) { }

  staffnameexist:boolean;

  usernameinvalid:boolean =true;
  usernamevalid:boolean =false;

  disable : boolean = false;
  nameinvalid : boolean = true;
  namevalid: boolean = false;
  passlen: boolean = false;

  lnameinvalid : boolean = true;
  lnamevalid: boolean = false;


  interestinvalid : boolean = true;
  interestvalid: boolean = false;

  phoneinvalid : boolean = true;
  phonevalid : boolean = false;

  emailinvalid : boolean = true;
  emailvalid : boolean = false;

  isAdmininvalid:boolean=true;
  isAdminvalid:boolean=false;

  passinvalid : boolean ;
  passvalid : boolean;

  phone_valid : any = /^[0-9]+$/;


  instructor : any = {username:'', firstname: '', lastname: '', gender: '',  interest: '', phone : '', email: '',password: '',isAdmin:''}
  instruct_array = [];
  id: number = 0;

  fname : any = /^[A-Z a-z]+$/;
  phone : any = /^[0-9]+$/;

  ngOnInit() {
    this.nameinvalid = true;
    this.namevalid = false;

    this.lnameinvalid = true;
    this.lnamevalid = false;

    this.interestinvalid = true;
    this.interestvalid = false;


    this.phoneinvalid = true;
    this.phonevalid = false;



    this.emailinvalid = true;
    this.emailvalid = false;

    this.isAdmininvalid=true;
    this.isAdminvalid=false;

    this.passinvalid;
    this.passvalid;

  }

  validatename(){
    if (this.instructor.firstname != null && this.instructor.firstname.match([/^[A-Z a-z]+$/])) {
      this.namevalid = true;
      this.nameinvalid = false;
    }
    else {
      this.namevalid = false;
      this.nameinvalid = true;
      this.disable = true;
    }
  }

  validatelname() {
    if (this.instructor.lastname != null && this.instructor.lastname.match(this.fname)) {
      this.lnamevalid = true;
      this.lnameinvalid = false;
    }
    else {
      this.lnamevalid = false;
      this.lnameinvalid = true;
      this.disable = true;
    }
  }

  validateinterest() {
    if (this.instructor.interest != null && this.instructor.interest.match(this.fname)) {
      this.interestvalid = true;
      this.interestinvalid = false;
    }
    else {
      this.interestvalid = false;
      this.interestinvalid = true;
      this.disable = true;
    }
  }


  validatephone(){
    if(this.instructor.phone!= null && this.instructor.phone.match(this.phone) && this.instructor.phone.length == 11){
      this.phonevalid = true;
      this.phoneinvalid = false;
    }
    else{
      this.phonevalid = false;
      this.phoneinvalid = true;
      this.disable = true;
    }
  }

  validateemail() {
    if (this.instructor.email != null && this.instructor.email.match(/^[A-z A-Z]*[0-9]*@[a-z A-Z]*[.][a-z A-Z]+$/)) {
      this.emailvalid = true;
      this.emailinvalid = false;
    }
    else {
      this.emailvalid = false;
      this.emailinvalid = true;
      this.disable = true;
    }
  }


  validateisAdmin() {
    if (this.instructor.isAdmin!= null && this.instructor.isAdmin.match("true")|| this.instructor.isAdmin.match("false")) {

      this.isAdminvalid  = true;
      this.isAdmininvalid= false;
    }
    else {
      this.isAdminvalid = false;
      this.isAdmininvalid = true;
      this.disable = true;
    }
  }


  validatepassword() {

    if (this.instructor.password != null && this.instructor.password.length >=8 )  {
      this.passvalid = true;
      this.passinvalid = false;
      this.passlen = false;
    }
    else {
      this.passvalid = false;
      this.passinvalid = true;
      this.passlen = true;
      this.disable = true;

    }
  }

  validatelength(){
    if(this.instructor.password.length <8){
      this.passinvalid = true;
      this.passlen = true;
    }
    else{
      this.passvalid = true;
      this.passlen = false;
      this.disable = true;
    }
  }



  create(){
    if(this.instructor.firstname!= null && this.instructor.firstname.match(this.fname) &&
      this.instructor.lastname!= null && this.instructor.lastname.match(this.fname)&&
      this.instructor.gender!= null && this.instructor.interest!= null && this.instructor.interest.match(this.fname) &&
      this.instructor.email!= null && this.instructor.phone != null && this.instructor.phone.length == 11 &&
      this.instructor.phone.match(this.phone_valid) && this.instructor.isAdmin!= null && this.instructor.isAdmin.match("true") ||
      this.instructor.isAdmin.match("false") && this.instructor.password != null && this.instructor.password.length >=7){

      this._httpService.postAddstaff(this.instructor).subscribe(
        data => console.log('data value:' + data.username),
        error => {
          // this.instructornameexist == true, this.usernameinvalid = true, this.usernamevalid = false
        },
        () => {

          $(".call_modal").show(function () {
            $(".modal").fadeIn();

          });

          $(document).ready(function () {
            $(".closer").click(function () {
              $(".modal").fadeOut();
            });
          });
        });

    }



    else if(this.instructor.password.length <8){
      console.log(this.instructor.password.length);
      this.passlen = false;
      this.passinvalid = true;
      this.passvalid = false;
      this.disable = true;
    }
  }

  validateusername():boolean {

    console.log('agaya');
    if (this.instructor.username != null && this.instructor.username.match(/^[A-Z a-z]+$/)) {
      console.log('ok');
      this.usernameinvalid = false;
      this.usernamevalid = true;
      return true;

    }
    else {
      this.usernameinvalid = true;
      this.usernamevalid = false;
      return false;
    }
  }

  Staffnameexist(): boolean
  {
    this._httpService.postCheckusername(this.instructor).subscribe(
      data => console.log('data value:' + data.username),
      error => {this.staffnameexist== true, this.usernameinvalid=true,this.usernamevalid=false},
      () =>{this.staffnameexist=false, this.usernamevalid=true, this.usernameinvalid = false}

    );

    if(this.staffnameexist== true)
    {
      return false;
    }
    else
    {
      return true;
    }

  }


}
