import { TestBed, inject } from '@angular/core/testing';

import { StaffserviceService } from './staffservice.service';

describe('StaffserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StaffserviceService]
    });
  });

  it('should ...', inject([StaffserviceService], (service: StaffserviceService) => {
    expect(service).toBeTruthy();
  }));
});
