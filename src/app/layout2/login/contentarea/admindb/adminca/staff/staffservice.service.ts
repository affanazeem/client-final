import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class StaffserviceService {

  constructor(private _http:Http) { }


  postCheckusername(staffusername :any)
  {
    console.log("Staff name"+ staffusername.username);
    var body = {username:staffusername.username};
    var headers =new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/Validstaffname' ,body,
      {
        headers:headers,
      }

    ).map(res => res.json())
  }




  postAddstaff(instructor :any)
  {
    var body = {username:instructor.username, firstname:instructor.firstname,
      lastname:instructor.lastname,gender:instructor.gender, interest:instructor.interest,phone:instructor.phone,
      email:instructor.email, password:instructor.password,isAdmin:instructor.isAdmin};
    var headers =new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/addStaff' ,body,
      {
        headers:headers,
      }

    ).map(res => res.json())
  }


}
