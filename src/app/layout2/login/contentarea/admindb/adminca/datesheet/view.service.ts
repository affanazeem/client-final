import { Injectable } from '@angular/core';
import {Http, Response, Headers} from "@angular/http";

@Injectable()
export class ViewService {

  classname: any;

  constructor(private _http: Http) {}

  classentered: any;

  classnameEntered(classname: any){

    this.classentered = classname;
    console.log(this.classentered);
  }

  validateteacher(teachername: any){
    console.log("teacher name: " + teachername)
    return this._http.get('http://localhost:3000/admins/validateTeacher/'+teachername).map((response:Response) => response.json());
  }

  getTeachers(){
    return this._http.get('http://localhost:3000/admins/viewAllTeacher').map((response:Response) => response.json());
  }

  validateSubjectName(firstname: any,classname:any,subjectname: any){
    console.log('in service');
    return this._http.get('http://localhost:3000/admins/validateCourses/'+firstname+'/'+classname+'/'+subjectname).map((response:Response) => response.json());
  }

  invigilate(v: any){

    console.log("firstname:"+v.firstname + "last name: "+ v.lastname + "gender: "+ v.gender);
    var body={username:v.username, firstname:v.firstname,lastname:v.lastname,gender:v.gender,phone:v.phone, email:v.email,password:v.password,isInvigilator:true};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/UpdateTeacher/'+v.id;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }

  makeInvigilator(v: any){

    console.log("firstname:"+v.firstname + "last name: "+ v.lastname + "gender: "+ v.gender);
    var body={username:v.username, firstname:v.firstname,lastname:v.lastname,gender:v.gender,phone:v.phone, email:v.email,password:v.password,isInvigilator:true};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = 'http://localhost:3000/admins/UpdateTeacher/'+v.id;
    return this._http.put(url,body,headers).map((response:Response) => response.json());

  }


  addDateSheet(dateexam:any, classname:any,tname:any,subjectname: any, time:any){

      var body = {dateexam:dateexam,classname:classname,tname:tname,subjectname:subjectname,time:time};
      var headers = new Headers();
      headers.append('Content-Type','application/json');

      return this._http.post('http://localhost:3000/admins/makedatesheet', body,
        {
          headers: headers,
        }).map(res => res.json())
  }

}
