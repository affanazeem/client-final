import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import {ViewService} from "./view.service";

@Component({
  selector: 'app-datesheet',
  templateUrl: './datesheet.component.html',
  styleUrls: ['./datesheet.component.css']
})
export class DatesheetComponent implements OnInit {

  datesheet: any = {date: '', classname:'', tname: '', subject: '',time:''};

  array_teacher : any = [];

  classEntered: any;

  subjectinvalid: boolean = false;
  tnameinvalid: boolean = false;
  nameinvalid: boolean = false;
  dateinvalid: boolean = false;
  classinvalid: boolean = false;
  viewDS: boolean = false;
  array_dates: any = [];

  array: any=[];

  constructor(private router: Router, private _httpService: ViewService) {
  }

  ngOnInit() {
    $(".modal_invalid").hide();


  }

  validateDate(): boolean{

    var dateexam = (<HTMLInputElement>document.getElementById('date')).value;
    console.log("date: "+dateexam);
    if(dateexam == ''){
      this.dateinvalid = true;
      return false;
    }
    else{
      this.dateinvalid = false;
      return true;
    }

  }



  validateClass():boolean{

    console.log("in validate class: ");

    if(this.datesheet.classname == ""){
      this.classinvalid = true;

    }
    else {

      if(this.datesheet.classname == "1" || this.datesheet.classname == "2" || this.datesheet.classname == "3" || this.datesheet.classname == "4"
      || this.datesheet.classname == "5"){
        this.datesheet.time = "09:00AM";
        this.classinvalid = false;
        console.log(this.datesheet.classname);
        return true;
      }

      else{
        this.datesheet.time = "05:00PM";
        this.classinvalid = false;
        console.log(this.datesheet.classname);
        return true;
      }


    }
  }

  view() {
    console.log('at view! ');
    this.viewDS = true;
    $(".call_modal_invoice").show(function () {
      $(".modal_invoice").fadeIn();
    });

    $(".closer_invoice").click(function () {
      $(".modal_invoice").fadeOut();
    });
  }


  teacherAPI() {
    this._httpService.validateteacher(this.datesheet.tname).subscribe(
      data => { this.array_teacher = data;
      console.log(this.array_teacher);

      },

      error => {
        this.tnameinvalid = true;
      },

      () => {
        this.tnameinvalid = false;
      }
    )

  }

    valstd():boolean
    {

      this._httpService.validateSubjectName(this.datesheet.tname, this.datesheet.classname, this.datesheet.subject).subscribe(
        data => {
          this.subjectinvalid = false;
          return true;
        },

        error => {
          this.subjectinvalid = true;
          return false;
          },

        () => {
          this.subjectinvalid = false;
          return true;
        }
      )

      if(this.subjectinvalid == true){
        return false;
      }
      else{
        return true;
      }
    }



  validateTname():boolean {
    if (this.datesheet.tname != null && this.datesheet.tname.match(/^[A-Z a-z]+$/) && this.teacherAPI()) {
      console.log('ok!');
      this.tnameinvalid = false;
      return true;
    }
    else {
      this.tnameinvalid = true;
      return false;
    }
  }

  validateSub() {

    console.log(this.datesheet.subject);
    if (this.datesheet.subject != null && this.datesheet.subject.match(/^[A-Z a-z]+$/) && this.valstd()== true) {
      console.log('ok!');
      this.subjectinvalid = false;
      return true;
    }
    else {
      this.subjectinvalid = true;
      return false;
    }
  }

ok(){
  $(".btn-warning").click(function () {
    $(".modal_invalid").fadeOut();
  });

}

  add() {

    if (this.datesheet.classname != null && this.datesheet.tname != null
      && this.datesheet.subject != "" && this.tnameinvalid == false && this.subjectinvalid == false
      && this.validateDate() == true && this.validateSub()==true ) {

      console.log('in if');
      this._httpService.addDateSheet(this.datesheet.date,this.datesheet.classname,this.datesheet.tname,this.datesheet.subject,this.datesheet.time).subscribe(
        data => {
        },

        error => {
          $(".call_modal_invalid").show(function () {
            $(".modal_invalid").fadeIn();
          })},

        () => {
        }
      )
    }

    else {
      $(".call_modal_invalid").show(function () {
        $(".modal_invalid").fadeIn();

      })
    }
  }


  one(){
  this.classEntered = "1";
  this._httpService.classnameEntered(this.classEntered);
  this.router.navigate(['/admindb/adminca/viewsheet']);
  }

  two(){

    this.classEntered = "2";
    this._httpService.classnameEntered(this.classEntered);
    this.router.navigate(['/admindb/adminca/viewsheet']);
  }
  three(){

    this.classEntered = "3";
    this._httpService.classnameEntered(this.classEntered);
    this.router.navigate(['/admindb/adminca/viewsheet']);
  }
  four(){

    this.classEntered = "4";
    this._httpService.classnameEntered(this.classEntered);
    this.router.navigate(['/admindb/adminca/viewsheet']);
  }
  five(){

    this.classEntered = "5";
    this._httpService.classnameEntered(this.classEntered);
    this.router.navigate(['/admindb/adminca/viewsheet']);
  }
  six(){

    this.classEntered = "6";

    this.router.navigate(['/admindb/adminca/viewsheet']);
  }
  seven(){

    this.classEntered = "7";
    this.router.navigate(['/admindb/adminca/viewsheet']);
  }
  eight(){

    this.classEntered = "8";
    this.router.navigate(['/admindb/adminca/viewsheet']);
  }
  nine(){

    this.classEntered = "9";
    this.router.navigate(['/admindb/adminca/viewsheet']);
  }
  matric(){

    this.classEntered = "10";
    this.router.navigate(['/admindb/adminca/viewsheet']);
  }


}

