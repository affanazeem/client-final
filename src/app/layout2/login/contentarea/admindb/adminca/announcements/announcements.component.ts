import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {AnnouncementsService} from "./announcements.service";
@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit {

  sendTo: string='';
  content: string='';
  initial: boolean = true;


  constructor(private _httpService: AnnouncementsService) { }

  ngOnInit() {

    $(".modal_invalid").hide();
    $(".modal_cinvalid").hide();
  }


  res(sendTo) {

    this.sendTo = sendTo;

    if (this.content == '' || this.content == null || this.content == "") {
      console.log('in if');
      $(".call_modal_cinvalid").show(function () {
        $(".modal_cinvalid").fadeIn();
      });
    }

    else
      if (sendTo == null || sendTo == "") {
        console.log('in if');
        $(".call_modal_invalid").show(function () {
          $(".modal_invalid").fadeIn();
        })

    }

    else {
      $(".call_modal_approve").show(function () {
        $(".modal_approve").fadeIn();
      });
      $(".closer_approve").click(function () {
        $(".modal_approve").fadeOut();
      });

    }
  }



  yesApprove(){

    $(".btn-blue").click(function () {
      $(".modal_approve").hide();
    });

    this._httpService.postAnnouncement(this.content, this.sendTo).subscribe(

      data => {},

      error => {},

      () =>{}

    )

    this.content= '';
    $(".modal_approve").hide();
  }


  noApprove(){
      $(".modal_approve").hide();
  }


  cancel() {
      $(".modal_invalid").hide();
  }


  ok() {
    $(".modal_cinvalid").hide();
  }
}
