import { Injectable } from '@angular/core';
import {Http, Response, Headers} from "@angular/http";

@Injectable()
export class AnnouncementsService {

  constructor(private _http: Http) { }

  postAnnouncement(content: any, sendto: any){
    console.log("Content"+content+"Send To: "+ sendto);
    var body={content:content, sendto:sendto};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/postannouncements', body,
      {
        headers: headers
      }).map(res =>res.json())
  }


}
