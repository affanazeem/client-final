import { TestBed, inject } from '@angular/core/testing';

import { SetinvService } from './setinv.service';

describe('SetinvService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetinvService]
    });
  });

  it('should ...', inject([SetinvService], (service: SetinvService) => {
    expect(service).toBeTruthy();
  }));
});
