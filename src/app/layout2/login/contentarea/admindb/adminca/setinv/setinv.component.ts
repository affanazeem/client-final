import { Component, OnInit } from '@angular/core';
import {InvigilatorService} from "../invigilator/invigilator.service";
import {SetinvService} from "./setinv.service";
import * as $ from 'jquery';

@Component({
  selector: 'app-setinv',
  templateUrl: './setinv.component.html',
  styleUrls: ['./setinv.component.css']
})
export class SetinvComponent implements OnInit {

  dateexam: any;
  time:any;
  classname: any;
  tname: any;
  subjectname: any;
  examId: any;


  a: any = {firstname: '', lastname: '', gender: '', phone: '', email: ''};
  inv = {firstname: '', lastname: '', gender: '', phone: '', email: ''};
  array: any = [];

  array_student: any = [];

  teacherId: any;

  constructor(private _viewInv: InvigilatorService, private _httpService: SetinvService) {

    this.classname = this._viewInv.classname;
    this.tname = this._viewInv.tname;
    this.subjectname = this._viewInv.subjectname;
    this.time = this._viewInv.time;
    this.dateexam = this._viewInv.dateexam;
    this.examId = this._viewInv.examId;


    console.log("in approve!");
    console.log(this.classname);
    console.log(this.tname);
    console.log(this.subjectname);
    console.log(this.time);
    console.log(this.dateexam);
    console.log(this.examId);

  }


  ngOnInit() {

    this._httpService.getAllTeachers().subscribe(
      data => {
        this.array = data

      },
    )
  }


  approve(v: any){

    this.inv.firstname = v.firstname;
    this.inv.lastname = v.lastname;
    this.inv.gender = v.gender;
    this.inv.phone = v.phone;
    this.inv.email = v.email;
    this.teacherId = v.id;

    $(".call_modal_approve").show(function () {
      $(".modal_approve").fadeIn();
    });
    $(".closer_approve").click(function () {
      $(".modal_approve").fadeOut();
    });

  }



  yesApprove(){
    console.log("firstname"+this.inv.firstname);
    console.log("lastname"+this.inv.lastname);
    console.log("gender"+this.inv.gender);
    console.log("phone"+this.inv.phone);
    console.log("email"+this.inv.email);

    $(".btn-blue").click(function () {
      $(".modal_approve").hide();
    });


      this._httpService.okApprove(this.examId,this.teacherId,this.inv.firstname, this.inv.lastname).subscribe(
        data => {this.array = data},

        ()=>{this._httpService.notify(this.tname,this.inv.firstname,this.classname,this.subjectname).subscribe(

          data =>{}
,
          error =>{console.log('error')}
,
          () => {},

        )

        }

      )

   }

  noApprove(){
    $(".btn-red").click(function () {
      console.log('no!');
      $(".modal_approve").fadeOut();
    });
  }


  delete(v: any) {
    console.log('outside for');
    for (var i = 0; i < this.array.length; i++) {
      console.log('in for');
      if (v.id == this.array[i].id) {
        this.array.splice(i, 1);
        this._httpService.DeleteRecord(v.id).subscribe(
          data => {
            // this.array = data
          },
          error => {
            console.log('error')
          },
          () => {
            console.log('complete')
          }
        )
      }
    }
  }
}


