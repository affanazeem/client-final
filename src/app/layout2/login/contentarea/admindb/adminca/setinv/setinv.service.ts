import { Injectable } from '@angular/core';
import {Http, Response, Headers} from "@angular/http";

@Injectable()
export class SetinvService {

  classname: any;
  subjectname: any;
  firstname: any;
  tname: any;

  constructor(private _http: Http) {
  }

  getAllTeachers() {

    let url = 'http://localhost:3000/admins/viewAllTeacher';
    return this._http.get(url).map((response: Response) => response.json());
  }


  DeleteRecord(v: number) {
    console.log('in delete service');
    let url = 'http://localhost:3000/admins/DeleteTeacher/' + v;
    return this._http.delete(url).map((response: Response) => response.json());
  }


  okApprove(examId: any, v: any, fn:any, ln:any) {

    var body = {datesheetId: examId, teacherId: v.id,firstname:fn,lastname:ln};
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/AssignInvigilators', body,
      {
        headers: headers,
      }).map(res => res.json())
  }


  notify(tname: any,firstname: any,classname:any,subjectname){

    this.classname = classname;
    this.subjectname = subjectname;
    this.firstname = firstname;
    this.tname = tname;

    console.log(this.classname);
    console.log(this.subjectname);

    console.log("in notification!");

    var body = {
      to:this.firstname,
      from: this.tname,
      classname: this.classname,
      subjectname:this.subjectname,
      type:"invigilator",
      expire:false
    };
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/teachers/sendNotification', body,
      {
        headers: headers,
      }).map(res => res.json())

  }


}
