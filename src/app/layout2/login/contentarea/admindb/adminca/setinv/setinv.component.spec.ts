import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetinvComponent } from './setinv.component';

describe('SetinvComponent', () => {
  let component: SetinvComponent;
  let fixture: ComponentFixture<SetinvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetinvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetinvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
