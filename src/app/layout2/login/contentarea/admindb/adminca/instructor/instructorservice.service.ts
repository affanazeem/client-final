import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class InstructorserviceService {

  constructor(private _http:Http) { }




  postCheckusername(instructorusername :any)
  {
    console.log("Instructor name"+ instructorusername.username);
    var body = {username:instructorusername.username};
    var headers =new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/Validinstructorname' ,body,
      {
        headers:headers,
      }

    ).map(res => res.json())
  }



  postAddteacher(instructor :any)
  {
    var body = {username:instructor.username, firstname:instructor.firstname,
      lastname:instructor.lastname,gender:instructor.gender,phone:instructor.phone,
      email:instructor.email, password:instructor.password,isInvigilator:instructor.isInvigilator,expire:false};
    var headers =new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post('http://localhost:3000/admins/addTeacher' ,body,
      {
        headers:headers,
      }

    ).map(res => res.json())
  }



}
