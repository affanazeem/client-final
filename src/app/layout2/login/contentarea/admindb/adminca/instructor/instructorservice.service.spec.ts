import { TestBed, inject } from '@angular/core/testing';

import { InstructorserviceService } from './instructorservice.service';

describe('InstructorserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstructorserviceService]
    });
  });

  it('should ...', inject([InstructorserviceService], (service: InstructorserviceService) => {
    expect(service).toBeTruthy();
  }));
});
