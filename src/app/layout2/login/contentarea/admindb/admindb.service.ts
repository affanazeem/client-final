import { Injectable } from '@angular/core';

import {Headers, Http, Response} from "@angular/http";

@Injectable()
export class AdmindbService {

  constructor(private _http:Http) { }


  getAllStudents(){
    let url = 'http://localhost:3000/admins/getAllResults/';
    return this._http.get(url).map((response:Response) => response.json());
  }

}
