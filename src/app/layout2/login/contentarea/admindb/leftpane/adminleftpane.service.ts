import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";

@Injectable()
export class AdminleftpaneService {

  constructor(private _http:Http) { }

  getLogoutAdmin(v)
  {
    console.log('logout service')
    return this._http.get('http://localhost:3000/admins/logoutAdmin').map((response:Response) => response.json());
  }

}
