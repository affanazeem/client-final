import { Component, OnInit } from '@angular/core';
import {Router} from  '@angular/router';
import {AdminleftpaneService} from "./adminleftpane.service";
@Component({
  selector: 'app-leftpane',
  templateUrl: './leftpane.component.html',
  styleUrls: ['./leftpane.component.css']
})
export class LeftpaneComponent implements OnInit {

  Re: boolean;


  constructor(private router:Router, private _httpService:AdminleftpaneService) { }


  Add()
  {
    console.log('ok');
    this.router.navigate(['/addteacher'])
  }
  Addstf()
  {
    this.router.navigate(['/addstaff'])
  }
  ngOnInit() {
  }


  logout(v)
  {
    console.log("logout ts")
    this._httpService.getLogoutAdmin(v).subscribe(
      data => {
        this.Re=data.Re
      },
      error => {

      },
      ()=> {
        if (this.Re == true) {
          this.router.navigate(['/layout1'])

        }

        console.log("logged out");
      }

    )

  }



}
