import { TestBed, inject } from '@angular/core/testing';

import { AdminleftpaneService } from './adminleftpane.service';

describe('AdminleftpaneService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminleftpaneService]
    });
  });

  it('should ...', inject([AdminleftpaneService], (service: AdminleftpaneService) => {
    expect(service).toBeTruthy();
  }));
});
