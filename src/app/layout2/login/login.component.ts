import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from "./login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],

})
export class LoginComponent implements OnInit {

  username : string;
  password: string;

  constructor(private router: Router,private _httpService: LoginService) { }

  ngOnInit() {
  }

  login() {

    var name = /^[A-Z a-z _]+$/;

    if (this.username != "" && this.username.match(name)) {
      console.log(this.username);
      console.log(this.password);
      this._httpService.details(this.username, this.password);

      this.router.navigate(['/dashboard'])
    }
    else{
      alert('invalid username!');
    }
  }

  return(){
    this.router.navigate(['/layout1']);
  }
}
