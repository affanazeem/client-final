import {Layout1Component} from "../layout1/layout1.component";
import { Injectable } from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class TestingService {

  username: any;
  password: any;
  post: any;

  constructor(private _http: Http) { }

  getCurrentTime(){
    console.log('okkk');

    return this._http.get('http://localhost:3000/admins/name').map(res => res.json());
  }


  details(Details:any, post:any){
    this.username = Details.username;
    this.password = Details.password;
    this.post = post;
    console.log("SERVICE'S USERNAME: "+Details.username+"SERVICE'S PASSWORD: "+Details.password+"SERVICE'S POST: "+post)
  }


  getDetails(){

    let url = 'http://localhost:3000/students/getAllDetails/' + this.username;
    return this._http.get(url).map((response: Response) => response.json());

  }


  postLogin(Admin: any){

  console.log('okkk');
  console.log("Admin username"+Admin.username + "Admin Password: "+Admin.password);
  var body={username:Admin.username, password:Admin.password};
  var headers = new Headers();
  headers.append('Content-Type','application/json');

  return this._http.post('http://localhost:3000/admins/loginAdmin', body,
    {
      headers: headers
    }).map(res =>res.json())
  }






  postInstructorLogin(teacher:any)
  {
    console.log('okkk');
    console.log("Instructor username"+teacher.username + "Admin Password: "+teacher.password);
    var body={username:teacher.username, password:teacher.password};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/teachers/loginteacher', body,
      {
        headers: headers
      }).map(res =>res.json())
  }

  postStaffLogin(staff:any)
  {
    console.log('okkk');
    console.log("Staff username"+staff.username + "Staff Password: "+staff.password);
    var body={username:staff.username, password:staff.password};
    var headers = new Headers();
    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/teachers/loginstaff', body,
      {
        headers: headers
      }).map(res =>res.json())
  }

  login(user:any, post:any,expire:any)
  {
    console.log("username"+user.username + "user Password: "+user.password+"post"+post);
    var body={username:user.username, password:user.password, post:post,expire:expire};
    var headers = new Headers();

    headers.append('Content-Type','application/json');

    return this._http.post('http://localhost:3000/admins/login', body,
      {
        headers: headers
      }).map(res =>res.json())
  }

  postStudentLogin(student:any)
  {
  let url = 'http://localhost:3000/students/loginstudent/' + student.username + '/' + student.password;
    return this._http.get(url).map((response: Response) => response.json());
  }


  checkLogin(username:any,password:any)
  {
    let url = 'http://localhost:3000/admins/loginUser/' + username + '/' +  password;
    return this._http.get(url).map((response: Response) => response.json());
  }

}
