import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import {TestingService} from "./testing.service";
import {InstructdbService} from "../layout2/login/contentarea/instructdb/instructdb.service";


@Component({
  selector: 'app-layout1',
  templateUrl: './layout1.component.html',
  styleUrls: ['./layout1.component.css']
})
export class Layout1Component implements OnInit {

  post: any;
  loginmatch: boolean = false;
  loginteachermatch: boolean;
  loginStaffmatch: boolean = false;
  loginStudentmatch: boolean = false;
  Admin: any = {username: '', password: ''};
  teacher: any = {username: '', password: ''};
  staffs: any = {username: '', password: ''};
  student: any = {username: '', password: ''};
  expire: boolean = false;
  array: any = [];
  initial : boolean=true;


  username: any;
  password: any;

  constructor(private router: Router, private _httpService: TestingService, private _Instruct: InstructdbService) {

    if(_Instruct.initial == false){
      this.initial = false;
      this.username = _Instruct.username;
      this.password = _Instruct.password;
    }

  }



  ngOnInit() {

    if (this.initial == false) {
      console.log("on ng init of layout1");
      this._httpService.checkLogin(this.username,this.password).subscribe(
        data => {
          this.array = data;
          this.expire = this.array.expire;
          console.log("expire value: " + this.expire);
          if (this.expire == true) {
            this.router.navigate(['/instructdb'])
          }
          else {
            this.router.navigate(['/layout1'])
          }
        },

        error => {
            // this._httpService.checkLogin(this.username,this.password).subscribe(
            //   data => {
            //     this.array = data
            //     this.expire = this.array.expire;
            //     console.log("expire value: " + this.expire);
            //     if (this.expire == true) {
            //       this.router.navigate(['/studentdb'])
            //     }
            //     else {
            //       this.router.navigate(['/layout1'])
            //     }
            //   },
            //   error => {
            //   },
            //   () => {
            //   },
            // )
          },
      )
    }
  }


  InstructorReg() {
    console.log('isn');
    this.router.navigate(['/instructor'])
  }


  staffReg() {
    this.router.navigate(['/instructor'])
  }

  adminReg() {
    this.router.navigate(['/instructor'])
  }

  Instructor() {
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
  }



  facultylogin() {
    this.post = "teacher";
    this._httpService.details(this.teacher, this.post);
    this._httpService.postInstructorLogin(this.teacher).subscribe(
      data => this.loginteachermatch = data.loginteachermatch,
      error => {this.loginteachermatch = true
        this.router.navigate(['/layout1']);
      },
      () => {
        this.expire = true;
        this.router.navigate(['/instructdb']);
        this._httpService.login(this.teacher, this.post, this.expire).subscribe(

          data => {
          },

          error => {
            this.router.navigate(['/layout1']);
          },

          () => { console.log('success!');
            this.router.navigate(['/instructdb']);
          },
        )

      }
    )

  }




  staff() {
    $(".call_modal_staff").show(function () {
      $(".modal_staff").fadeIn();
    });
    $(".closer_staff").click(function () {
      $(".modal_staff").fadeOut();
    });
  }

  stafflogin() {
    this.post = "staff";
    console.log('here!');
    this._httpService.details(this.staffs, this.post);
    this._httpService.postStaffLogin(this.staffs).subscribe(
      data => this.loginStaffmatch = data.loginStaffmatch,
      error => this.loginStaffmatch = true,
      () => {
        () => {
          this.expire = true;
          this.router.navigate(['/staffdb']);
          this._httpService.login(this.staff, this.post, this.expire).subscribe(

            data => {
            },

            error => {
              this.router.navigate(['/layout1']);
            },

            () => { console.log('success!');
              this.router.navigate(['/staffdb']);

            },
          )}
      })

  }


  studentlogin() {
    this.post = "student";

    this._httpService.details(this.student, this.post);
    this._httpService.postStudentLogin(this.student).subscribe(
      data => this.loginStudentmatch = data.loginStudentmatch,
      error => {this.loginStudentmatch = true
        this.router.navigate(['/layout1']);
      },
      () => {
        this.expire = true;
        this.router.navigate(['/studentdb']);
        this._httpService.login(this.student, this.post, this.expire).subscribe(

          data => {
          },

          error => {
            this.router.navigate(['/layout1']);
          },

          () => { console.log('success!');
            this.router.navigate(['/studentdb']);

          },
        )

      }
    )

  }






  admin() {
    $(".call_modal").show(function () {
      $(".modal").fadeIn();

    });
    $(".closer").click(function () {
      $(".modal").fadeOut();
    });
  }

  std() {
    console.log('in std');
    $(".call_modal_student").show(function () {
      $(".modal_student").fadeIn();

    });
    $(".closer_student").click(function () {
      $(".modal_student").fadeOut();
    });
  }


  postAdminLogin() {
    this.post = "admin";
    console.log('here!');
    this._httpService.details(this.Admin, this.post);

    this._httpService.postLogin(this.Admin).subscribe(
      data => this.loginmatch = data.loginmatch,
      error => {this.loginmatch = true
        this.router.navigate(['/layout1']);
      },
      () => {
        this.expire = true;
        this.router.navigate(['/admindb']);
        this._httpService.login(this.Admin, this.post, this.expire).subscribe(

          data => {
          },

          error => {
           this.router.navigate(['/layout1']);
          },

          () => { console.log('success!');
            this.router.navigate(['/admindb']);

          },
        )

      }
   )

  }
}
