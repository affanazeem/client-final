import { Component, OnInit } from '@angular/core';
import {TestingService} from "../testing.service";

@Component({
  selector: 'app-httptest',
  templateUrl: './httptest.component.html',
  styleUrls: ['./httptest.component.css']
})
export class HttptestComponent implements OnInit {

  getData: string;
  postData: string;

  constructor(private _httpService: TestingService) { }

  ngOnInit() {
  }


  onTestGet(){
    console.log('here!');
    this._httpService.getCurrentTime().subscribe(
      data =>  this.getData = data.name ,

      error => console.log(error),
      () => console.log('yahoo!')

    )
  }


  onTestPost(){
    // console.log('here!');
    // this._httpService.postLogin().subscribe(
    //   data =>  this.getData = data.myname ,
    //    error => console.log(error),
    //   () => console.log('yahoo!')
    //
    // )
  }



}
